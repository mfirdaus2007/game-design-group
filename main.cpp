#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glaux.lib")
#include "main.h"										// This includes our header file
#include "Md2.h"										// Include the MD2 header file.
#include "glm.h"
#include <sstream>
#include <string>
#include <iostream>
#define ILUT_USE_OPENGL	// This MUST be defined before calling the DevIL headers or we don't get OpenGL functionality
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

using namespace std;
enum state{
	start,option,menu,highscore,help,storyscene
};
struct particlespawn
{
	GLfloat x, y, z;
	GLfloat minxd, minyd, minzd;
	GLfloat maxxd, maxyd, maxzd;
	
	GLfloat gravity;

	GLfloat minr, ming, minb;
	GLfloat maxr, maxg, maxb;

	GLfloat rboundx, rboundy, rboundz;
	GLfloat lboundx, lboundy, lboundz;
};
struct particle
{
	GLfloat x,y,z;
	GLfloat r,g,b;
	GLfloat xd,yd,zd;
	GLfloat yr;
};
struct vertex
{
	GLfloat x, y, z;
	GLfloat u, v;
};
struct triangle
{
	int texture;
	bool anim;
	int frame;
	vertex v[3];
};
struct quadric
{
	GLfloat x, y, z;
	int texture;
	int type;
	GLUquadricObj *quadratic;
	GLfloat radius, height, rotation;
};
struct tile
{
	// Graphics
	int numtriangles;
	triangle* t;
	int numquadrics;
	quadric* q;
	bool useparticle;
	particlespawn ps;
	int numparticles;
	particle* p;
	// Game Interaction
	bool passable;
	int monster_code;
};
struct level
{
	// Actual Level
	int width, height;
	int t[MAX_GRID][MAX_GRID];
	int monster_code[MAX_GRID][MAX_GRID];
	// Graphical Effects
	GLfloat fogdist;
	GLfloat fogcolor[4];
	GLfloat fogdensity;

	GLfloat skycolor[4];
	
	int weather; // Going to be implemented in a reaaaaaaaaally long time.	
};
struct Bullet
{
	GLfloat xpos, zpos;
};
struct Fruitman
{
	GLfloat xpos, zpos;
	GLfloat ypos;
	GLfloat direction; //Rotation direction
		
	int ammo;
	int live;

	float speed;
	char * model;
	int texure;
	float scale;

	int runframe_from;
	int runframe_to;
};
int time_score_flood=10;
float camera_z_offset=-9.0f;
float camera_direction;
struct Camera
{
	//4 point to character to check collision
	GLfloat xpos1, zpos1;
	GLfloat xpos2, zpos2;
	GLfloat xpos3, zpos3;
	GLfloat xpos4, zpos4;

	GLfloat xpos, zpos;
	GLfloat ypos;
	GLfloat direction; //Rotation direction
};
struct Monster
{
	GLfloat xpos, zpos,ypos;
	GLfloat scale;
	int type;
	GLfloat speed;
	GLfloat direction; //Rotation direction
	char* model;
	int texure;

	int runframe_from;
	int runframe_to;
	int monster_code;
	bool live;
};
typedef struct polygon
{
     bool over; /* TRUE if polygon exists */
	 float x1,y1,x2,y2; /* world coordinate */
     int xmin, xmax, ymin, ymax; /* bounding box */
};

state CurrentState = menu;

GLuint loadImage(const char* theFileName);
void InitMenu();
UINT g_Texture[MAX_TEXTURES] = {0};						// This holds the texture info, referenced by an ID
GLuint MenuFruitTexture;	
GLuint MenuItemsTexture;
GLuint MainTitleTexture;
GLuint scoreetc;
GLuint languages[2];
GLuint win;
int language_used=0;
GLuint HelpTexture;
int   g_ViewMode	  = GL_TRIANGLES;					// We want the default drawing mode to be normal
bool  g_bLighting     = true;							// Turn lighting on initially
float g_RotateX		  = 0.0f;							// This is the current value at which the model is rotated
float g_RotationSpeed = 0.5f;							// This is the speed that our model rotates.  (-speed rotates left)
HDC			hDC=NULL;								// General HDC - (handle to device context)
HGLRC		hRC=NULL;								// General OpenGL_DC - Our Rendering Context for OpenGL
HWND		hWnd;									// This is the handle for the window
HINSTANCE	hInstance;		
bool	active=true;
bool	fullscreen=false;
int gamemode=1;
const float piover180 = 0.0174532925f;
const int TIME_CHANGE_MODE = 50;
int screen_width;
int screen_height;
int screennum=0;
long selectedMenu=-1;
DWORD ticks=0;
long int frames=0;
int fps=0;
boolean started=false;
POINT point;
int direction=0;
static float elapsedTime[MAX_MONSTER];
static float lastTime[MAX_MONSTER];
GLMmodel* pmodel1[3];
int number_of_monster=0;
int number_of_fruit=0;
int number_of_fruit_eaten=0;
long score=0;
Monster list_monster[MAX_MONSTER];
Monster list_monster_temp[MAX_MONSTER];
CLoadMD2 g_LoadMd2;	
t3DModel list_monster_3DModel[MAX_MONSTER];	
t3DModel fruitman3DModel;
tile deftiles[10];
GLuint texture[15];
level curlvl;
Camera mainc;
Fruitman fruitman;
Fruitman bullet;
int timer=10000;
Camera mainc_temp;
Fruitman fruitman_temp;
level curlvl_temp;
float camera_ypos_default=-9.7f;
float camera_zpos_default=4.0f;
float gamemode_camera_angle_default=45.0f;
bool change_mode=false;
float gamemode_camera_angle=gamemode_camera_angle_default;
float camera_ypos=camera_ypos_default;
float camera_xpos;
float camera_zpos;

float sky_rotate;
float scale=22.0f;
float sky_ypos;
float sky_zpos;
int temp_direction[MAX_MONSTER];
polygon polygons[4];

GLuint HighScoreTitleTexture;
GLuint bg;
GLuint numbers[11];
GLuint story[4];
int curentStory=0;
polygon buttonHighScore[2];

void DrawCubeFace(float fSize);
void InitHighScore();

float gamemode_camera_angle_old;
float gamemode_camera_angle_new;

float camera_xpos_old;
float camera_xpos_new;

float camera_ypos_old;
float camera_ypos_new;
float camera_zpos_old;
float camera_zpos_new;

float camera_direction_old;
float camera_direction_new;

float sky_rotate_old;
float sky_rotate_new;

float sky_ypos_new;
float sky_ypos_old;

float sky_zpos_new;
float sky_zpos_old;

void scoringText();


bool check_bounding(const polygon button,long x, long y)
{
   /* find first polygon in which we are in bounding box */
      if((y>=button.ymin)&&(y<button.ymax)&&(x>=button.xmin)&&(x<=button.xmax))
           {   
				return true;			   
           }
	  else
		  return false;
}
bool flood=false;
void drawtile(tile t) 
{

	int i;
	// First step: Drawing triangles
	int wall__height_scale=1;
	glColor3f(1.0f,1.0f,1.0f);
	//glEnable(GL_TEXTURE_2D);
	for ( i=0;i<t.numtriangles;i++)
	{
		glBindTexture(GL_TEXTURE_2D,texture[t.t[i].texture]);
		if(flood==true)
		{
			if(texture[t.t[i].texture]==3)
			{
				glBindTexture(GL_TEXTURE_2D,5);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D,texture[t.t[i].texture]);
			}

		}
		else
		{
			glBindTexture(GL_TEXTURE_2D,texture[t.t[i].texture]);
		}
		glBegin(GL_TRIANGLES);			
			glTexCoord2f(t.t[i].v[0].u,t.t[i].v[0].v);
			glVertex3f(t.t[i].v[0].x/wall__height_scale,t.t[i].v[0].y/wall__height_scale,1-t.t[i].v[0].z/wall__height_scale);
			
			glTexCoord2f(t.t[i].v[1].u,t.t[i].v[1].v);
			glVertex3f(t.t[i].v[1].x/wall__height_scale,t.t[i].v[1].y/wall__height_scale,1-t.t[i].v[1].z/wall__height_scale);
			
			glTexCoord2f(t.t[i].v[2].u,t.t[i].v[2].v);
			glVertex3f(t.t[i].v[2].x/wall__height_scale,t.t[i].v[2].y/wall__height_scale,1-t.t[i].v[2].z/wall__height_scale);	
		glEnd();		
	}

	
}
void createtile(int tilenumber, tile & temp)
{
	tile t=deftiles[tilenumber];
	temp.numparticles=t.numparticles;
	temp.numquadrics=t.numquadrics;
	temp.numtriangles=t.numparticles;
	temp.ps=t.ps;
	temp.useparticle=t.useparticle;
	srand(time(NULL));
	if (temp.useparticle)
	{
		temp.p=new particle[temp.numparticles];
		for(int i=0;i<temp.numparticles;i++)
		{
			temp.p[i].x=temp.ps.x;
			temp.p[i].y=temp.ps.y;
			temp.p[i].z=temp.ps.z;
			// Now, to calculate the particle's new speed vectors
			float randv=(rand()/32767.0f);
			// Now, the rand is a number between 0 and 1
			temp.p[i].xd=(temp.ps.minxd-temp.ps.maxxd)*randv+temp.ps.minxd;
			// Rinse, lather, repeat. #-)
			randv=(rand()/32767.0f);
			temp.p[i].yd=(temp.ps.minyd-temp.ps.maxyd)*randv+temp.ps.minxd;
			randv=(rand()/32767.0f);
			temp.p[i].zd=(temp.ps.minzd-temp.ps.maxzd)*randv+temp.ps.minzd;
			randv=(rand()/32767.0f);
			temp.p[i].r=(temp.ps.minr-temp.ps.maxr)*randv+temp.ps.minr;
			randv=(rand()/32767.0f);
			temp.p[i].g=(temp.ps.ming-temp.ps.maxg)*randv+temp.ps.ming;
			randv=(rand()/32767.0f);
			temp.p[i].b=(temp.ps.ming-temp.ps.maxg)*randv+temp.ps.minb;
		}
	}
	temp.t=new triangle[temp.numtriangles];
	int m;
	for ( m=0;m<temp.numtriangles;m++)
	{
		temp.t[m]=t.t[m];
	}
	temp.q=new quadric[temp.numquadrics];
	for (m=0;m<temp.numquadrics;m++)
	{
		temp.q[m]=t.q[m];
	}
}
void initdeftiles()
{
	ifstream fin("levels/tiles.txt");
	int numtiles=0;
	int usepar=0;
	fin>>numtiles;	
	for (int i=0;i<numtiles;i++)
	{
		fin>>usepar;
		deftiles[i].passable=bool(usepar);
		fin>>deftiles[i].numtriangles;
		deftiles[i].t = new triangle[deftiles[i].numtriangles];
		for (int i2=0;i2<deftiles[i].numtriangles;i2++)
		{
			fin>>deftiles[i].t[i2].texture;
			fin>>deftiles[i].t[i2].v[0].x>>deftiles[i].t[i2].v[0].y>>deftiles[i].t[i2].v[0].z>>deftiles[i].t[i2].v[0].u>>deftiles[i].t[i2].v[0].v;
			fin>>deftiles[i].t[i2].v[1].x>>deftiles[i].t[i2].v[1].y>>deftiles[i].t[i2].v[1].z>>deftiles[i].t[i2].v[1].u>>deftiles[i].t[i2].v[1].v;
			fin>>deftiles[i].t[i2].v[2].x>>deftiles[i].t[i2].v[2].y>>deftiles[i].t[i2].v[2].z>>deftiles[i].t[i2].v[2].u>>deftiles[i].t[i2].v[2].v;			
		}		
	}
	fin.close();
}
float fruitman_xpos_defaut;
float fruitman_zpos_defaut;
bool createlevel(char * filename, level & l) 
{
	
	ifstream fin(filename);
	fin>>l.width>>l.height;
	int temp;
	for (int w=0;w<l.width;w++)
	for (int h=0;h<l.height;h++)		
	{
		fin>>temp;
		if(temp<5)   //Maze
		{
			l.t[w][h]=temp;
		}
		else if(temp==5)  //Fruit Man
		{			
			camera_xpos=h+0.5;
			camera_zpos=w+2.0f;
			mainc.direction=0.0f;
			gamemode_camera_angle=gamemode_camera_angle_default;
			camera_ypos=camera_ypos_default;
			camera_zpos=w-camera_zpos_default;

			fruitman.xpos=h;
			fruitman.zpos=w;

			fruitman_xpos_defaut=h;
			fruitman_zpos_defaut=w;

			fruitman.direction=0.0f;
			fruitman.ypos=-8.0f;
			fruitman.ammo=0;
			fruitman.live=10;

			fruitman.speed=0.0;
			fruitman.model="Model/EGGM.MD2";
			fruitman.texure=19;
			fruitman.scale=0.011;

			mainc_temp=mainc;
			fruitman_temp=fruitman;
			
			sky_rotate=50.0f;
			sky_ypos=mainc.ypos-scale-4.0f;
			sky_zpos=mainc.zpos/5-scale-14.0f;

		}
		else     //Monster
		{
			l.t[w][h]=temp;
			Monster temp_monster;
			temp_monster.type=temp;
			temp_monster.xpos=h;
			temp_monster.zpos=w;
			list_monster[number_of_monster]=temp_monster;			
			if(temp==12) //Monster type=6 RED ENEMY    
			{				
				list_monster[number_of_monster].speed=0.08;
				list_monster[number_of_monster].model="Model/Yoshi.MD2";
				list_monster[number_of_monster].texure=17;
				list_monster[number_of_monster].scale=0.027;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=3;
				list_monster[number_of_monster].runframe_to=10;
			}
			else if(temp==20) //Monster Sonic    //Slow but can shoot
			{				
				list_monster[number_of_monster].speed=0.03;
				list_monster[number_of_monster].model="Model/Sonic.MD2";
				list_monster[number_of_monster].texure=14;
				list_monster[number_of_monster].scale=0.018;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=3;
				list_monster[number_of_monster].runframe_to=10;
			}
			else if(temp==8) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/COWZ.MD2";
				list_monster[number_of_monster].texure=16;
				list_monster[number_of_monster].scale=0.16;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=3;
			}
			else if(temp==9) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/Mario.MD2";
				list_monster[number_of_monster].texure=13;
				list_monster[number_of_monster].scale=0.02;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=3;
				list_monster[number_of_monster].runframe_to=10;
			}
			else if(temp==10) //Monster 
			{				
				list_monster[number_of_monster].speed=0.0;
				list_monster[number_of_monster].model="Model/bananatree.MD2";
				list_monster[number_of_monster].texure=4;
				list_monster[number_of_monster].scale=0.05;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==11) //Monster 
			{				
				list_monster[number_of_monster].speed=0.0;
				list_monster[number_of_monster].model="Model/fence_a.MD2";
				list_monster[number_of_monster].texure=19;
				list_monster[number_of_monster].scale=0.0000;
				list_monster[number_of_monster].direction=0;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=3;
				list_monster[number_of_monster].runframe_to=10;
			}
			else if(temp==14) //Monster 
			{				
				list_monster[number_of_monster].speed=0.0;
				list_monster[number_of_monster].model="Model/POSS.MD2";
				list_monster[number_of_monster].texure=20;
				list_monster[number_of_monster].scale=0.053;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=1;
			}
			else if(temp==13) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/BUS2.md2";
				list_monster[number_of_monster].texure=21;
				list_monster[number_of_monster].scale=0.053;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==15) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/EMER.md2";
				list_monster[number_of_monster].texure=22;
				list_monster[number_of_monster].scale=0.023;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==16) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/EMMY.md2";
				list_monster[number_of_monster].texure=22;
				list_monster[number_of_monster].scale=0.023;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==17) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/FWR2.md2";
				list_monster[number_of_monster].texure=23;
				list_monster[number_of_monster].scale=0.023;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==20) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/STPT.md2";
				list_monster[number_of_monster].texure=24;
				list_monster[number_of_monster].scale=0.013;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==18) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/SIGN.md2";
				list_monster[number_of_monster].texure=25;
				list_monster[number_of_monster].scale=0.013;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=0;
			}
			else if(temp==6) //Monster 
			{				
				list_monster[number_of_monster].speed=0.05;
				list_monster[number_of_monster].model="Model/CHIC.MD2";
				list_monster[number_of_monster].texure=15;
				list_monster[number_of_monster].scale=0.15;
				list_monster[number_of_monster].ypos=-1.0f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=1;
			}
			else if(temp==7) //Monster Pink
			{				
				list_monster[number_of_monster].speed=0.03;
				list_monster[number_of_monster].model="Model/BUNY.MD2";
				list_monster[number_of_monster].texure=7;
				list_monster[number_of_monster].scale=0.15;
				list_monster[number_of_monster].ypos=-0.84f;
				list_monster[number_of_monster].runframe_from=0;
				list_monster[number_of_monster].runframe_to=1;
			}

			list_monster[number_of_monster].monster_code=w*100+h;
			list_monster[number_of_monster].live=true;
			number_of_monster++;

			if((temp>=6&&temp<=9)||temp==11)
			{
				number_of_fruit++;
			}			
		}
	}	

	fin>>l.skycolor[0]>>l.skycolor[1]>>l.skycolor[2]>>l.skycolor[3];
	fin>>l.fogcolor[0]>>l.fogcolor[1]>>l.fogcolor[2]>>l.fogcolor[3];
	fin>>l.fogdist;
	fin>>l.fogdensity;
	fin>>l.weather;
	curlvl_temp=curlvl;
	

	return true;
}
void tileupdate(tile & t)
{
	srand(rand());
	timer++;
	if (timer==10000)
		timer=0;
	for (int m=0;m<t.numtriangles;m++)
	{
		if (t.t[m].anim)

		if (timer%30==0)
			t.t[m].frame++;
		if ((t.t[m].frame>2)||(t.t[m].frame<0))
			t.t[m].frame=0;
	}
	if (t.useparticle)
	{
		for (int m=0;m<t.numparticles;m++)
		{
			t.p[m].x+=t.p[m].xd;
			t.p[m].y+=t.p[m].yd;
			t.p[m].z+=t.p[m].zd;
			t.p[m].yd-=t.ps.gravity;
			t.p[m].yr+=1.0f;
			if ((t.p[m].x<t.ps.lboundx)|| // Basically, if the particle is outside
				(t.p[m].x>t.ps.rboundx)|| // the spawn point's logical box
				(t.p[m].y<t.ps.lboundy)||
				(t.p[m].y>t.ps.rboundy)||
				(t.p[m].z<t.ps.lboundz)||
				(t.p[m].z>t.ps.rboundz))
			{
				t.p[m].x=t.ps.x;
				t.p[m].y=t.ps.y;
				t.p[m].z=t.ps.z;
				// Now, to calculate the particle's new speed vectors
				float randv=(rand()%10000);
				randv /= 10000.0f;
				// Now, the rand is a number between 0 and 1
				t.p[m].xd=(t.ps.maxxd-t.ps.minxd)*randv+t.ps.minxd;
				// Rinse, lather, repeat. #-)
				randv=(rand()%10000);
				randv /= 10000.0f;
				t.p[m].yd=(t.ps.minyd-t.ps.maxyd)*randv+t.ps.minxd;
				randv=(rand()%10000);
				randv /= 10000.0f;
				t.p[m].zd=(t.ps.maxzd-t.ps.minzd)*randv+t.ps.minzd;
				randv=(rand()%10000);
				randv /= 10000.0f;
				t.p[m].r=(t.ps.maxr-t.ps.minr)*randv+t.ps.minr;
				randv=(rand()%10000);
				randv /= 10000.0f;
				t.p[m].g=(t.ps.maxg-t.ps.ming)*randv+t.ps.ming;
				randv=(rand()%10000);
				randv /= 10000.0f;
				t.p[m].b=(t.ps.maxg-t.ps.ming)*randv+t.ps.minb;
				t.p[m].yr=0.0f;
			}
		}		
	}
}

float round(float number)
{
    return number < 0.0 ? ceil(number - 0.5) : floor(number + 0.5);
}

void move2(Camera &c)
{
	static int lasttime=0;
	int move=timeGetTime()-lasttime;
	lasttime=timeGetTime();
	float movement=move/25.0f;
	if (GetAsyncKeyState(VK_LEFT))
	{
		c.direction-=3*movement;
	}
	if (GetAsyncKeyState(VK_RIGHT))
	{
		c.direction+=3*movement;
	}
	if(c.direction>=360)
	{
		c.direction=0;
	}
	if(c.direction<0)
	{
		c.direction=c.direction+360;
	}
	float fruit_man_radius=0.2; 
	if (GetAsyncKeyState(VK_UP))
	{
		c.xpos+=sin(c.direction*piover180)*.05f*movement;
		c.zpos+=cos(c.direction*piover180)*.05f*movement;

		c.xpos1=c.xpos+fruit_man_radius; //UP
		c.zpos1=c.zpos;
		c.xpos2=c.xpos; //LEFT
		c.zpos2=c.zpos+fruit_man_radius;
		c.xpos3=c.xpos-fruit_man_radius; //DOWN
		c.zpos3=c.zpos;
		c.xpos4=c.xpos; //RIGHT
		c.zpos4=c.zpos-fruit_man_radius;

		if (!deftiles[curlvl.t[(int)c.zpos1][(int)c.xpos1]].passable||!deftiles[curlvl.t[(int)c.zpos2][(int)c.xpos2]].passable||!deftiles[curlvl.t[(int)c.zpos3][(int)c.xpos3]].passable||!deftiles[curlvl.t[(int)c.zpos4][(int)c.xpos4]].passable)   //Can not pass
		{
			c.xpos-=sin(c.direction*piover180)*.05f*movement; //Come back
			c.zpos-=cos(c.direction*piover180)*.05f*movement; //Come back
		}
	}
	if (GetAsyncKeyState(VK_DOWN))
	{
		c.xpos-=sin(c.direction*piover180)*.05f*movement;
		c.zpos-=cos(c.direction*piover180)*.05f*movement;
		
		c.xpos1=c.xpos+fruit_man_radius; //UP
		c.zpos1=c.zpos;
		c.xpos2=c.xpos; //LEFT
		c.zpos2=c.zpos+fruit_man_radius;
		c.xpos3=c.xpos-fruit_man_radius; //DOWN
		c.zpos3=c.zpos;
		c.xpos4=c.xpos; //RIGHT
		c.zpos4=c.zpos-fruit_man_radius;

		if (!deftiles[curlvl.t[(int)c.zpos1][(int)c.xpos1]].passable||!deftiles[curlvl.t[(int)c.zpos2][(int)c.xpos2]].passable||!deftiles[curlvl.t[(int)c.zpos3][(int)c.xpos3]].passable||!deftiles[curlvl.t[(int)c.zpos4][(int)c.xpos4]].passable)   //Can not pass
		{
			c.xpos+=sin(c.direction*piover180)*.05f*movement;
			c.zpos+=cos(c.direction*piover180)*.05f*movement;
		}
	}
	int t=round(c.direction/90);

	fruitman.direction=t*90;
	
	
	fruitman.xpos=c.xpos-0.5f;
	fruitman.zpos=c.zpos-0.5f;
}

void LoadGLTextures()                                    // Load Bitmaps And Convert To Textures
{
		char input[200];
		char buffer[200];
	
		int num=0;

		ifstream fin("graphics/textures.txt");
		fin>>num;
		for (int i=0;i<num;i++)
		{
			fin>>input;
			wsprintf(buffer,"graphics/%s",input);
			AUX_RGBImageRec *TextureImage[1];               // Create Storage Space For The Texture

			memset(TextureImage,0,sizeof(void *)*1);        // Set The Pointer To NULL

			// Load The Bitmap, Check For Errors, If Bitmap's Not Found Quit

			if (TextureImage[0]=auxDIBImageLoad(buffer))
			{
					glGenTextures(1, &texture[i]);          // Create Three Textures

					glBindTexture(GL_TEXTURE_2D, texture[i]);
					glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
					gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
			}
			if (TextureImage[0])                            // If Texture Exists
			{
					if (TextureImage[0]->data)              // If Texture Image Exists
					{
							free(TextureImage[0]->data);    // Free The Texture Image Memory
					}

					free(TextureImage[0]);                  // Free The Image Structure
			}
		}
}
int InitGL(void)									
{
	
	createlevel("levels/level1.txt",curlvl);
	LoadGLTextures();
	glShadeModel(GL_SMOOTH);						
	glClearColor(0.0f, 0.0f,0.0f, 1.0f);				
	glClearDepth(1.0f);		
	glEnable(GL_DEPTH_TEST);							
	glDepthFunc(GL_LEQUAL);	
	//glMouseFunc(myMouse);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	InitMenu();
	InitHighScore();
	// Initialise all DevIL functionality
	ilutRenderer(ILUT_OPENGL);
	ilInit();
	iluInit();
	ilutInit();
	ilutRenderer(ILUT_OPENGL);	// Tell DevIL that we're using OpenGL for our rendering
	
	MenuFruitTexture= loadImage("Menu/Menu.png");
	MainTitleTexture= loadImage("Menu/MenuTitleFruit.png");
	HighScoreTitleTexture= loadImage("Menu/highscore.png");
	MenuItemsTexture= loadImage("Menu/MenuItems.png");

	languages[0]= loadImage("Menu/english.png");
	languages[1]= loadImage("Menu/chinese.png");
	win= loadImage("Menu/win.png");

	numbers[0]=loadImage("Menu/0.png");
	numbers[1]=loadImage("Menu/1.png");
	numbers[2]=loadImage("Menu/2.png");
	numbers[3]=loadImage("Menu/3.png");
	numbers[4]=loadImage("Menu/4.png");
	numbers[5]=loadImage("Menu/5.png");
	numbers[6]=loadImage("Menu/6.png");
	numbers[7]=loadImage("Menu/7.png");
	numbers[8]=loadImage("Menu/8.png");
	numbers[9]=loadImage("Menu/9.png");
	numbers[10]=loadImage("Menu/dot.png");
	bg=loadImage("Menu/bg.png");
	story[0]=loadImage("Menu/story1.png");
	story[1]=loadImage("Menu/story2.png");
	story[2]=loadImage("Menu/story3.png");
	story[3]=loadImage("Menu/story4.png");

	HelpTexture=loadImage("Menu/help.png");
	scoreetc=loadImage("Menu/lifeammofruitscore.png");

	CurrentState=menu;
	mciSendString("open sound/menu.mp3 type mpegvideo alias song1", NULL, 0, 0); 
	mciSendString("play song1 repeat", NULL, 0, 0);
	mciSendString("open sound/eat.wav type mpegvideo alias eat", NULL, 0, 0);
	mciSendString("open sound/fire.wav type mpegvideo alias fire", NULL, 0, 0);
//mciSendString("close song1", NULL, 0, 0);
	//if(CurrentState!=start)
	//mciSendString("play repeat sound/menu.wav", 0, 0, 0);
		//PlaySound("sound/menu.wav", NULL, SND_ASYNC|SND_FILENAME|SND_LOOP|SND_NOWAIT);

	initdeftiles();
	ticks=GetTickCount();



	return true;
}
void KillGLWindow(void)							
{
	if (fullscreen)									
	{
		ChangeDisplaySettings(NULL,0);			
		ShowCursor(true);					
	}

	if (hRC)										
	{
		if (!wglMakeCurrent(NULL,NULL))				
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))					
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;									
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;									
	}

	if (hWnd && !DestroyWindow(hWnd))					
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;									
	}

	if (!UnregisterClass("OpenGL",hInstance))		
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;								
	}
}
BOOL CreateGLWindow(char* title, int width, int height, int bits, bool fullscreenflag)
{
	GLuint		PixelFormat;		
	WNDCLASS	wc;						
	DWORD		dwExStyle;				
	DWORD		dwStyle;			
	RECT		WindowRect;				
	WindowRect.left=(long)0;		
	WindowRect.right=(long)width;	
	WindowRect.top=(long)0;			
	WindowRect.bottom=(long)height;	

	fullscreen=fullscreenflag;			

	hInstance			= GetModuleHandle(NULL);			
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc		= (WNDPROC) WinProc;				
	wc.cbClsExtra		= 0;									
	wc.cbWndExtra		= 0;								
	wc.hInstance		= hInstance;						
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);		
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);		
	wc.hbrBackground	= NULL;								
	wc.lpszMenuName		= NULL;								
	wc.lpszClassName	= "OpenGL";						

	if (!RegisterClass(&wc))								
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;										
	}
	
	if (fullscreen)											
	{
		DEVMODE dmScreenSettings;							
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		
		dmScreenSettings.dmPelsWidth	= width;			
		dmScreenSettings.dmPelsHeight	= height;			
		dmScreenSettings.dmBitsPerPel	= bits;				
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			if (MessageBox(NULL,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","NeHe GL",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				fullscreen=false;	
			}
			else
			{
				MessageBox(NULL,"Program Will Now Close.","ERROR",MB_OK|MB_ICONSTOP);
				return false;								
			}
		}
	}

	if (fullscreen)											
	{
		dwExStyle=WS_EX_APPWINDOW;								
		dwStyle=WS_POPUP;									
		ShowCursor(false);									
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;		
		dwStyle=WS_OVERLAPPEDWINDOW;						
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, false, dwExStyle);	


	if (!(hWnd=CreateWindowEx(	dwExStyle,							
								"OpenGL",						
								title,							
								dwStyle |							
								WS_CLIPSIBLINGS |				
								WS_CLIPCHILDREN,				
								0, 0,								
								WindowRect.right-WindowRect.left,	
								WindowRect.bottom-WindowRect.top,
								NULL,							
								NULL,							
								hInstance,						
								NULL)))							
	{
		KillGLWindow();							
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;							
	}

	static	PIXELFORMATDESCRIPTOR pfd=			
	{
		sizeof(PIXELFORMATDESCRIPTOR),			
		1,										
		PFD_DRAW_TO_WINDOW |					
		PFD_SUPPORT_OPENGL |					
		PFD_DOUBLEBUFFER,						
		PFD_TYPE_RGBA,								
		bits,									
		0, 0, 0, 0, 0, 0,						
		0,										
		0,										
		0,										
		0, 0, 0, 0,								
		16,									  
		0,										
		0,											
		PFD_MAIN_PLANE,							
		0,										
		0, 0, 0									
	};
	
	if (!(hDC=GetDC(hWnd)))						
	{
		KillGLWindow();						
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;							
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	
	{
		KillGLWindow();								
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;						
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))
	{
		KillGLWindow();							
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;							
	}

	if (!(hRC=wglCreateContext(hDC)))			
	{
		KillGLWindow();								
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;							
	}

	if(!wglMakeCurrent(hDC,hRC))					
	{
		KillGLWindow();								
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;					
	}

	ShowWindow(hWnd,SW_SHOW);					
	SetForegroundWindow(hWnd);					
	SetFocus(hWnd);	

	ReSizeGLScene1(width, height);				

	if (!InitGL())									
	{
		KillGLWindow();							
		MessageBox(NULL,"Initialization Failed.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;							
	}

	glEnable(GL_TEXTURE_2D);							// Enables Texture Mapping
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	//glEnable(GL_LIGHT0);								// Turn on a light with defaults set
//	glEnable(GL_LIGHTING);								// Turn on lighting
	glEnable(GL_COLOR_MATERIAL);						// Allow color

	//glEnable(GL_CULL_FACE);								// Turn culling on
	//glCullFace(GL_FRONT);								// Quake2 uses front face culling apparently
	glEnable(GL_POINT_SMOOTH); 
	glEnable(GL_LINE_SMOOTH); 
	glEnable(GL_POLYGON_SMOOTH); 
	glEdgeFlag(GL_FALSE); 
	ReSizeGLScene1(width, height);					// Setup the screen translations and viewport
	return true;								
}
void CreateTexture(UINT textureArray[], int textureID)
{
	// Generate a texture with the associative texture ID stored in the array
	glGenTextures(1, &textureArray[textureID]);
	// This sets the alignment requirements for the start of each pixel row in memory.
	glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
	// Bind the texture to the texture arrays index and init the texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	// Build Mipmaps (builds different versions of the picture for distances - looks better)
	//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, pBitmap->sizeX, pBitmap->sizeY, GL_RGB, GL_UNSIGNED_BYTE, pBitmap->data);

	// Lastly, we need to tell OpenGL the quality of our texture map.  GL_LINEAR_MIPMAP_LINEAR
	// is the smoothest.  GL_LINEAR_MIPMAP_NEAREST is faster than GL_LINEAR_MIPMAP_LINEAR, 
	// but looks blochy and pixilated.  Good for slower computers though.  
		
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);


}
bool bSetupPixelFormat(HDC hdc) 
{ 
    PIXELFORMATDESCRIPTOR pfd; 
    int pixelformat; 
 
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);			// Set the size of the structure
    pfd.nVersion = 1;									// Always set this to 1
														// Pass in the appropriate OpenGL flags
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; 
    pfd.dwLayerMask = PFD_MAIN_PLANE;					// We want the standard mask (this is ignored anyway)
    pfd.iPixelType = PFD_TYPE_RGBA;						// We want RGB and Alpha pixel type
    pfd.cColorBits = SCREEN_DEPTH;						// Here we use our #define for the color bits
    pfd.cDepthBits = SCREEN_DEPTH;						// Depthbits is ignored for RGBA, but we do it anyway
    pfd.cAccumBits = 0;									// No special bitplanes needed
    pfd.cStencilBits = 0;								// We desire no stencil bits
 
	// This gets us a pixel format that best matches the one passed in from the device
    if ( (pixelformat = ChoosePixelFormat(hdc, &pfd)) == FALSE ) 
    { 
        MessageBox(NULL, "ChoosePixelFormat failed", "Error", MB_OK); 
        return FALSE; 
    } 
 
	// This sets the pixel format that we extracted from above
    if (SetPixelFormat(hdc, pixelformat, &pfd) == FALSE) 
    { 
        MessageBox(NULL, "SetPixelFormat failed", "Error", MB_OK); 
        return FALSE; 
    } 
 
    return TRUE;										// Return a success!
}

void RenderSkybox()
{	
	glLoadIdentity();
	glColor4f(1.0, 1.0, 1.0,1.0f); 
	glPushMatrix();
	
	glTranslatef(0,sky_ypos,sky_zpos);
	glRotatef(sky_rotate,1,0,0);
	glScalef(scale,scale,scale);
	glRotatef(mainc.direction/scale/4,0,1,0); 
	float cz = -0.0f,cx = 1.0f;
	glBindTexture(GL_TEXTURE_2D,10);
	glBegin(GL_QUADS);	
		glTexCoord2f(cx, cz); glVertex3f(-1.0f,1.0f,-1.0f);
		glTexCoord2f(cx, cx); glVertex3f(-1.0f,1.0f, 1.0f);
		glTexCoord2f(cz, cx); glVertex3f( 1.0f,1.0f, 1.0f); 
		glTexCoord2f(cz, cz); glVertex3f( 1.0f,1.0f,-1.0f);
	glEnd(); 
	glPopMatrix();
 
};
void Cube(float scale,float xpos, float zpos)
{	
	glEnable(GL_BLEND);
	glColor4f(1.0f,1.0f,0.0f,0.7f);
	glLoadIdentity();
	glRotatef(mainc.direction,0.0f,1.0f,0.0f);				
	glTranslatef(xpos-mainc.xpos+0.5f,mainc.ypos+scale,(-zpos)+mainc.zpos-0.5f);
	glRotatef(direction--,0.0f,1.0f,0.0f);
	glScalef(scale,scale,scale); 
	float r = 1.0f; 
	glBegin(GL_QUADS);	
		glVertex3f(-r,1.0f,-r);
		glVertex3f(-r,1.0f, r);
		glVertex3f( r,1.0f, r); 
		glVertex3f( r,1.0f,-r);
	glEnd();
	glBegin(GL_QUADS);		
		glVertex3f(-r,-1.0f,-r);
		glVertex3f(-r,-1.0f, r);
		glVertex3f( r,-1.0f, r); 
		glVertex3f( r,-1.0f,-r);
	glEnd();
	glBegin(GL_QUADS);		
		glVertex3f(-1.0f, -r, r);	
		glVertex3f(-1.0f,  r, r); 
		glVertex3f(-1.0f,  r,-r);
		glVertex3f(-1.0f, -r,-r);		
	glEnd();
	// Common Axis X - Right side
	glBegin(GL_QUADS);		
		glVertex3f(1.0f, -r, r);	
		glVertex3f(1.0f,  r, r); 
		glVertex3f(1.0f,  r,-r);
		glVertex3f(1.0f, -r,-r);
	glEnd();
	// Common Axis Y - Draw Up side
	glBegin(GL_QUADS);		
		glVertex3f( r, -r, 1.0f);
		glVertex3f( r,  r, 1.0f);
		glVertex3f(-r,  r, 1.0f);
		glVertex3f(-r, -r, 1.0f);
	glEnd();
	//// Common Axis Y - Down side
	glBegin(GL_QUADS);		
		glVertex3f( r, -r, -1.0f);
		glVertex3f( r,  r, -1.0f);
		glVertex3f(-r,  r, -1.0f);
		glVertex3f(-r, -r, -1.0f);
	glEnd(); 
};

float ReturnCurrentTime(int nextFrame, int i)
{


	// This function is very similar to finding the frames per second.
	// Instead of checking when we reach a second, we check if we reach
	// 1 second / our animation speed. (1000 ms / kAnimationSpeed).
	// That's how we know when we need to switch to the next key frame.
	// In the process, we get the t value for how we are at to going to the
	// next animation key frame.  We use time to do the interpolation, that way
	// it runs the same speed on any persons computer, regardless of their specs.
	// It might look chopier on a junky computer, but the key frames still be
	// changing the same time as the other persons, it will just be not as smooth
	// of a transition between each frame.  The more frames per second we get, the
	// smoother the animation will be.

	// Get the current time in milliseconds
	float time = GetTickCount();

	// Find the time that has elapsed since the last time that was stored
	elapsedTime[i] = time - lastTime[i];

	// To find the current t we divide the elapsed time by the ratio of 1 second / our anim speed.
	// Since we aren't using 1 second as our t = 1, we need to divide the speed by 1000
	// milliseconds to get our new ratio, which is a 5th of a second.
	float t = elapsedTime[i] / (1000.0f / kAnimationSpeed);
	
	
	// If our elapsed time goes over a 5th of a second, we start over and go to the next key frame
	if (elapsedTime[i] >= (1000.0f / kAnimationSpeed) )
	{
		// Set our current frame to the next key frame (which could be the start of the anim)
		list_monster_3DModel[i].currentFrame = nextFrame;

		// Set our last time to the current time just like we would when getting our FPS.
		lastTime[i] = time;
	}

	// Return the time t so we can plug this into our interpolation.
	return t;
}
void AnimateMD2Model(int i)
{
	if(list_monster_3DModel[i].pObject.size() <= 0) return;
	
	// Here we grab the current animation that we are on from our model's animation list
	if(list_monster[i].runframe_to!=0)
	{
		tAnimationInfo *pAnim = &(list_monster_3DModel[i].pAnimations[list_monster_3DModel[i].currentAnim]);
	}
	
	// This gives us the current frame we are on.  We mod the current frame plus
	// 1 by the current animations end frame to make sure the next frame is valid.
	// If the next frame is past our end frame, then we go back to zero.  We check this next.

	int nextFrame = list_monster_3DModel[i].currentFrame + 1;

	// If the next frame is zero, that means that we need to start the animation over.
	// To do this, we set nextFrame to the starting frame of this animation.
	if(nextFrame > list_monster[i].runframe_to) 
		nextFrame = list_monster[i].runframe_from;
	
	// Get the current key frame we are on
	t3DObject *pFrame =		 &list_monster_3DModel[i].pObject[list_monster_3DModel[i].currentFrame];
	
	// Get the next key frame we are interpolating too
	t3DObject *pNextFrame =  &list_monster_3DModel[i].pObject[nextFrame];
	
	// Get the first key frame so we have an address to the texture and face information
	t3DObject *pFirstFrame = &list_monster_3DModel[i].pObject[0];
	
	// Next, we want to get the current time that we are interpolating by.  Remember,
	// if t = 0 then we are at the beginning of the animation, where if t = 1 we are at the end.
	// Anyhing from 0 to 1 can be thought of as a percentage from 0 to 100 percent complete.
	float t = ReturnCurrentTime(nextFrame,i);
	t=0;
	glColor3f(1.0f,1.0f,1.0f);
	// Render lines or normal triangles mode, depending on the global variable

	CreateTexture(g_Texture,list_monster[i].texure);			
		
	// Start rendering lines or triangles, depending on our current rendering mode (Lft Mouse Btn)
	glBegin(g_ViewMode);

		// Go through all of the faces (polygons) of the current frame and draw them
		for(int j = 0; j < pFrame->numOfFaces; j++)
		{
			// Go through each corner of the triangle and draw it.
			for(int whichVertex = 0; whichVertex < 3; whichVertex++)
			{
				// Get the index for each point of the face
				int vertIndex = pFirstFrame->pFaces[j].vertIndex[whichVertex];

				// Get the index for each texture coordinate for this face
				int texIndex  = pFirstFrame->pFaces[j].coordIndex[whichVertex];
						
				// Make sure there was a UVW map applied to the object.  Notice that
				// we use the first frame to check if we have texture coordinates because
				// none of the other frames hold this information, just the first by design.
				if(pFirstFrame->pTexVerts) 
				{
					// Pass in the texture coordinate for this vertex
					glTexCoord2f(pFirstFrame->pTexVerts[ texIndex ].x, 
								 pFirstFrame->pTexVerts[ texIndex ].y);
				}

				// Now we get to the interpolation part! (*Bites his nails*)
				// Below, we first store the vertex we are working on for the current
				// frame and the frame we are interpolating too.  Next, we use the
				// linear interpolation equation to smoothly transition from one
				// key frame to the next.
				
				// Store the current and next frame's vertex
				CVector3 vPoint1 = pFrame->pVerts[ vertIndex ];
				CVector3 vPoint2 = pNextFrame->pVerts[ vertIndex ];

				// By using the equation: p(t) = p0 + t(p1 - p0), with a time t
				// passed in, we create a new vertex that is closer to the next key frame.
				glVertex3f(vPoint1.x + t * (vPoint2.x - vPoint1.x), // Find the interpolated X
						   vPoint1.y + t * (vPoint2.y - vPoint1.y), // Find the interpolated Y
						   vPoint1.z + t * (vPoint2.z - vPoint1.z));// Find the interpolated Z
			}
		}

	// Stop rendering the triangles
	glEnd();	
}
void AnimateFruitMan()
{
	if(fruitman3DModel.pObject.size() <= 0) return;
	
	// Here we grab the current animation that we are on from our model's animation list
	if(fruitman.runframe_to!=0)
	{
		tAnimationInfo *pAnim = &(fruitman3DModel.pAnimations[fruitman3DModel.currentAnim]);
	}
	
	// This gives us the current frame we are on.  We mod the current frame plus
	// 1 by the current animations end frame to make sure the next frame is valid.
	// If the next frame is past our end frame, then we go back to zero.  We check this next.

	int nextFrame = fruitman3DModel.currentFrame + 1;

	// If the next frame is zero, that means that we need to start the animation over.
	// To do this, we set nextFrame to the starting frame of this animation.
	if(nextFrame > fruitman.runframe_to) 
		nextFrame = fruitman.runframe_from;
	
	// Get the current key frame we are on
	t3DObject *pFrame =		 &fruitman3DModel.pObject[fruitman3DModel.currentFrame];
	
	// Get the next key frame we are interpolating too
	t3DObject *pNextFrame =  &fruitman3DModel.pObject[nextFrame];
	
	// Get the first key frame so we have an address to the texture and face information
	t3DObject *pFirstFrame = &fruitman3DModel.pObject[0];
	
	// Next, we want to get the current time that we are interpolating by.  Remember,
	// if t = 0 then we are at the beginning of the animation, where if t = 1 we are at the end.
	// Anyhing from 0 to 1 can be thought of as a percentage from 0 to 100 percent complete.
	float t = ReturnCurrentTime(nextFrame,0); //TODO
	t=0;
	glColor3f(1.0f,1.0f,1.0f);
	// Render lines or normal triangles mode, depending on the global variable

	CreateTexture(g_Texture,fruitman.texure);			
		
	// Start rendering lines or triangles, depending on our current rendering mode (Lft Mouse Btn)
	glBegin(g_ViewMode);

		// Go through all of the faces (polygons) of the current frame and draw them
		for(int j = 0; j < pFrame->numOfFaces; j++)
		{
			// Go through each corner of the triangle and draw it.
			for(int whichVertex = 0; whichVertex < 3; whichVertex++)
			{
				// Get the index for each point of the face
				int vertIndex = pFirstFrame->pFaces[j].vertIndex[whichVertex];

				// Get the index for each texture coordinate for this face
				int texIndex  = pFirstFrame->pFaces[j].coordIndex[whichVertex];
						
				// Make sure there was a UVW map applied to the object.  Notice that
				// we use the first frame to check if we have texture coordinates because
				// none of the other frames hold this information, just the first by design.
				if(pFirstFrame->pTexVerts) 
				{
					// Pass in the texture coordinate for this vertex
					glTexCoord2f(pFirstFrame->pTexVerts[ texIndex ].x, 
								 pFirstFrame->pTexVerts[ texIndex ].y);
				}

				// Now we get to the interpolation part! (*Bites his nails*)
				// Below, we first store the vertex we are working on for the current
				// frame and the frame we are interpolating too.  Next, we use the
				// linear interpolation equation to smoothly transition from one
				// key frame to the next.
				
				// Store the current and next frame's vertex
				CVector3 vPoint1 = pFrame->pVerts[ vertIndex ];
				CVector3 vPoint2 = pNextFrame->pVerts[ vertIndex ];

				// By using the equation: p(t) = p0 + t(p1 - p0), with a time t
				// passed in, we create a new vertex that is closer to the next key frame.
				glVertex3f(vPoint1.x + t * (vPoint2.x - vPoint1.x), // Find the interpolated X
						   vPoint1.y + t * (vPoint2.y - vPoint1.y), // Find the interpolated Y
						   vPoint1.z + t * (vPoint2.z - vPoint1.z));// Find the interpolated Z
			}
		}

	// Stop rendering the triangles
	glEnd();	
}
bool shot=false;
int bullet_long=0;
bool eat_playsound =false;
void play_eat()
{
	if(eat_playsound)
	{
		mciSendString("close eat",NULL,0,0);
		mciSendString("open sound/eat.wav type mpegvideo alias eat", NULL, 0, 0);
		mciSendString("play eat", NULL, 0, 0);
		eat_playsound=false;
	}
}
bool scoring=false;
void move1(Fruitman &c)
{
	static int lasttime=0;
	int move=timeGetTime()-lasttime;
	lasttime=timeGetTime();
	float fruit_man_radius=0.2; 
	if (GetAsyncKeyState(VK_UP))
	{
		started=true;
		c.direction=0;	
	}
	else if (GetAsyncKeyState(VK_DOWN))
	{
		started=true;
		c.direction=180;
	}
	else if (GetAsyncKeyState(VK_LEFT))
	{
		started=true;
		c.direction=270;		
	}
	else if (GetAsyncKeyState(VK_RIGHT))
	{
		started=true;
		c.direction=90;
	}
	else if (GetAsyncKeyState(VK_SPACE)&&shot==false&&fruitman.ammo>0)
	{
		shot=true;
		bullet_long=0;
		//mciSendString("close fire", NULL, 0, 0);
		//mciSendString("open sound/fire.wav type mpegvideo alias fire", NULL, 0, 0);
		//mciSendString("play fire", NULL, 0, 0);
		scoring=true;

		PlaySound("Sound/fire.wav", NULL, SND_ASYNC);
		fruitman.ammo--;
	}
	if(shot==true)
	{
		bullet_long++;
		
		bullet.xpos=c.xpos;
		bullet.zpos=c.zpos;
		if(c.direction==0)
		{			
			bullet.zpos=c.zpos+bullet_long/4.0f;
			if(curlvl.t[(int)bullet.zpos+1][(int)bullet.xpos]==1)
			{
				bullet_long=0;
				shot=false;
			}
		}
		if(c.direction==180)
		{			
			bullet.zpos=c.zpos-bullet_long/4.0f;
			if(curlvl.t[(int)bullet.zpos][(int)bullet.xpos]==1)
			{
				bullet_long=0;
				shot=false;
			}
		}
		if(c.direction==90)
		{			
			bullet.xpos=c.xpos+bullet_long/4.0f;
			if(curlvl.t[(int)bullet.zpos][(int)bullet.xpos+1]==1)
			{
				bullet_long=0;
				shot=false;
			}
		}
		if(c.direction==270)
		{			
			bullet.xpos=c.xpos-bullet_long/4.0f;
			if(curlvl.t[(int)bullet.zpos][(int)bullet.xpos]==1)
			{
				bullet_long=0;
				shot=false;
				Cube(0.1,0,0);
			}
		}
		Cube(0.1,bullet.xpos,bullet.zpos);

		for(int i=0;i<number_of_monster;i++) //Draw monster
		{
			if((int)list_monster[i].xpos==(int)bullet.xpos&&(int)list_monster[i].zpos==(int)bullet.zpos&&list_monster[i].live==true)
			{
				list_monster[i].live=false;
				PlaySound("Sound/die.wav", NULL, SND_ASYNC);				
				if(scoring)
				{
					scoring=false;
						score+=25;
				}
			}
		}	
	}

	float movement=1/5.0f;
	float camera_scale=1.1;

	float old_zpos=fruitman.zpos;
	float old_xpos=fruitman.xpos;
	float old_zpos_camera=mainc.zpos;
	float old_xpos_camera=mainc.xpos;
	boolean checkco=true;
	if(fruitman.xpos<=0||fruitman.xpos>MAX_GRID||fruitman.zpos<=0||fruitman.zpos>MAX_GRID)
	{
		checkco=false;
	}
	else
	{
		checkco=true;
	}
	if(started==true)
	{
		if(GetAsyncKeyState(VK_RIGHT))	
		{
			fruitman.zpos=round(fruitman.zpos);
			fruitman.xpos=fruitman.xpos+movement;
			mainc.xpos=mainc.xpos+movement/camera_scale;
			if(checkco&&curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos+1]==1)
			{
				fruitman.xpos=fruitman.xpos-movement;
				mainc.xpos=mainc.xpos-movement/camera_scale;
				fruitman.zpos=round(fruitman.zpos);
				fruitman.xpos=round(fruitman.xpos);
			}
		}
		else if (GetAsyncKeyState(VK_LEFT))
		{
			fruitman.zpos=round(fruitman.zpos);
			fruitman.xpos=fruitman.xpos-movement;
			mainc.xpos=mainc.xpos-movement/camera_scale;
			if(checkco&&curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]==1)
			{
				fruitman.xpos=fruitman.xpos+movement;
				mainc.xpos=mainc.xpos+movement/camera_scale;
				fruitman.zpos=round(fruitman.zpos);
				fruitman.xpos=round(fruitman.xpos);
			}
		}
		else if (GetAsyncKeyState(VK_UP))
		{
			fruitman.xpos=round(fruitman.xpos);
			fruitman.zpos=fruitman.zpos+movement;
			mainc.zpos=mainc.zpos+movement/camera_scale;
			if(checkco&&(curlvl.t[(int)fruitman.zpos+1][(int)fruitman.xpos]==1||(curlvl.t[(int)fruitman.zpos+1][(int)fruitman.xpos]==2&&number_of_fruit!=number_of_fruit_eaten)))
			{
				fruitman.zpos=fruitman.zpos-movement;
				mainc.zpos=mainc.zpos-movement/camera_scale;
				fruitman.zpos=round(fruitman.zpos);
				fruitman.xpos=round(fruitman.xpos);
			}
		}
		else if (GetAsyncKeyState(VK_DOWN))
		{
			fruitman.xpos=round(fruitman.xpos);
			fruitman.zpos=fruitman.zpos-movement;
			mainc.zpos=mainc.zpos-movement/camera_scale;
			if(checkco&&!deftiles[curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]].passable&&(curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]==1))
			{
				mainc.zpos=mainc.zpos+movement/camera_scale;
				fruitman.zpos=fruitman.zpos+movement;
				fruitman.zpos=round(fruitman.zpos);
				fruitman.xpos=round(fruitman.xpos);
			}
		}
	}
	glLoadIdentity();			
	glTranslatef(fruitman.xpos-mainc.xpos+0.5f,mainc.ypos,(-fruitman.zpos)+mainc.zpos-0.5f);
	glScalef(fruitman.scale,fruitman.scale,fruitman.scale);
	glRotatef(90-fruitman.direction,0.0f,1.0f,0.0f);
	AnimateFruitMan();

	if(checkco==true)
	{
		if(curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]==0)
		{
			curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]=-1;
			//eat_playsound=true;
			//PlaySound("Sound/eat.wav", NULL, SND_ASYNC);
			fruitman.ammo++;
			score+=5;
		}

		if(curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]==7)
		{
			curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]=-1;
			if(language_used==0)
			{
				PlaySound("Sound/apple.wav", NULL, SND_ASYNC);
			}else
			{
				PlaySound("Sound/c_apple.wav", NULL, SND_ASYNC);
			}
			number_of_fruit_eaten++;
			score+=50;
		}
		if(curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]==6||curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]==11)
		{
			curlvl.t[(int)fruitman.zpos][(int)fruitman.xpos]=-1;
			if(language_used==0)
			{
				PlaySound("Sound/banana.wav", NULL, SND_ASYNC);
			}else
			{
				PlaySound("Sound/c_banana.wav", NULL, SND_ASYNC);
			}
			number_of_fruit_eaten++;
			score+=50;
		}



	}

}
void MonsterChangeDirection(int i)
{
	int r=rand() % 4 ;
	temp_direction[i]=r*90;
	list_monster[i].zpos=round(list_monster[i].zpos);
	list_monster[i].xpos=round(list_monster[i].xpos);
}
void MoveMonster(int i)
{
	
	if(temp_direction[i]==0)	
	{
		list_monster[i].xpos=list_monster[i].xpos+list_monster[i].speed;
		if((curlvl.t[(int)list_monster[i].zpos][(int)list_monster[i].xpos+1]==1)||(curlvl.monster_code[(int)list_monster[i].zpos][(int)list_monster[i].xpos+1]!=0&&curlvl.monster_code[(int)list_monster[i].zpos][(int)list_monster[i].xpos+1]!=list_monster[i].monster_code))
		{
			MonsterChangeDirection(i);
		}
		else
		{
			list_monster[i].direction=temp_direction[i];
		}
	}
	else if (temp_direction[i]==180)
	{
		list_monster[i].xpos-=list_monster[i].speed;
		if((curlvl.t[(int)list_monster[i].zpos][(int)list_monster[i].xpos]==1)||(curlvl.monster_code[(int)list_monster[i].zpos][(int)list_monster[i].xpos]!=0&&curlvl.monster_code[(int)list_monster[i].zpos][(int)list_monster[i].xpos]!=list_monster[i].monster_code))
		{
			MonsterChangeDirection(i);
		}
		else
		{
			list_monster[i].direction=temp_direction[i];
		}
	}
	else if (temp_direction[i]==90)
	{
		list_monster[i].zpos+=list_monster[i].speed;
		if(curlvl.t[(int)list_monster[i].zpos+1][(int)list_monster[i].xpos]==1||curlvl.t[(int)list_monster[i].zpos+1][(int)list_monster[i].xpos]==2||(curlvl.monster_code[(int)list_monster[i].zpos+1][(int)list_monster[i].xpos]!=0&&curlvl.monster_code[(int)list_monster[i].zpos+1][(int)list_monster[i].xpos]!=list_monster[i].monster_code))
		{
			MonsterChangeDirection(i);
		}
		else
		{
			list_monster[i].direction=temp_direction[i];
		}
	}
	else if (temp_direction[i]==270)
	{
		list_monster[i].zpos-=list_monster[i].speed;
		if((curlvl.t[(int)list_monster[i].zpos][(int)list_monster[i].xpos]==1)||(curlvl.monster_code[(int)list_monster[i].zpos][(int)list_monster[i].xpos]!=0&&curlvl.monster_code[(int)list_monster[i].zpos][(int)list_monster[i].xpos]!=list_monster[i].monster_code))
		{
			MonsterChangeDirection(i);
		}
		else
		{
			list_monster[i].direction=temp_direction[i];
		}
	}
}
void DrawMD2(int i) 
{
	glLoadIdentity();	
	glRotatef(mainc.direction,0.0f,1.0f,0.0f);				
	glTranslatef(list_monster[i].xpos-mainc.xpos+0.5f,mainc.ypos,(-list_monster[i].zpos)+mainc.zpos-0.5f);
	glScalef(list_monster[i].scale,list_monster[i].scale,list_monster[i].scale);
	glRotatef(list_monster[i].direction,0.0f,1.0f,0.0f);
	AnimateMD2Model(i);
}

void DrawMD2(int i, int xpos, int zpos) 
{
	glLoadIdentity();	
	glRotatef(mainc.direction,0.0f,1.0f,0.0f);				
	glTranslatef(xpos-mainc.xpos+0.5f,mainc.ypos,(-zpos)+mainc.zpos-0.5f);
	glScalef(list_monster[i].scale,list_monster[i].scale,list_monster[i].scale);
	glRotatef(list_monster[i].direction,0.0f,1.0f,0.0f);
	AnimateMD2Model(i);
}
void DrawOBJ(int i, char * model, GLfloat scale,int xpos, int zpos)
{
	glLoadIdentity ();
	
	glRotatef(mainc.direction,0.0f,1.0f,0.0f);	
	glTranslatef(xpos-mainc.xpos+0.5f,mainc.ypos+scale,(-zpos)+mainc.zpos-0.5f);
	glRotatef(0.01*direction--,0.0f,1.0f,0.0f);
	//glRotatef(list_monster[i].direction,0.0f,1.0f,0.0f);

	// Load the model only if it hasn't been loaded before
	// If it's been loaded then pmodel1 should be a pointer to the model geometry data...otherwise it's null
    if (!pmodel1[i]) 
	{
		// this is the call that actualy reads the OBJ and creates the model object
		
        pmodel1[i] = glmReadOBJ(model);	
        if (!pmodel1) exit(0);
		// This will rescale the object to fit into the unity matrix
		// Depending on your project you might want to keep the original size and positions you had in 3DS Max or GMAX so you may have to comment this.
        
		glmUnitize(pmodel1[i]);
		glmScale(pmodel1[i], scale);
		
		// These 2 functions calculate triangle and vertex normals from the geometry data.
		// To be honest I had some problem with very complex models that didn't look to good because of how vertex normals were calculated
		// So if you can export these directly from you modeling tool do it and comment these line
		// 3DS Max can calculate these for you and GLM is perfectly capable of loading them
        glmFacetNormals(pmodel1[i]);        
		glmVertexNormals(pmodel1[i], 90.0);
    }
    // This is the call that will actualy draw the model
	// Don't forget to tell it if you want textures or not :))
    glmDraw(pmodel1[i], GLM_SMOOTH| GLM_TEXTURE);
	
}
GLuint loadImage(const char* theFileName)
{
	ILuint imageID;				// Create an image ID as a ULuint
 
	GLuint textureID;			// Create a texture ID as a GLuint
 
	ILboolean success;			// Create a flag to keep track of success/failure
 
	ILenum error;				// Create a flag to keep track of the IL error state
 
	ilGenImages(1, &imageID); 		// Generate the image ID
 
	ilBindImage(imageID); 			// Bind the image
 
	success = ilLoadImage(theFileName); 	// Load the image file
 
	// If we managed to load the image, then we can start to do things with it...
	if (success)
	{
		std::cout << "Texture load successful." << std::endl;
		// The image is always flipped (i.e. upside-down and mirrored, flip it the right way up!)
		iluFlipImage();
		

		// Convert the image into a suitable format to work with
		// NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
 
		// Quit out if we failed the conversion
		if (!success)
		{
			error = ilGetError();
			std::cout << "Image conversion failed - IL reports error: " << error << " - " << iluErrorString(error) << std::endl;
			exit(-1);
		}
 
		// Generate a new texture
		glGenTextures(1, &textureID);
 
		// Bind the texture to a name
		glBindTexture(GL_TEXTURE_2D, textureID);
 
		// Set texture clamping method
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
 
		// Set texture interpolation method to use linear interpolation (no MIPMAPS)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
 
		// Specify the texture specification
		glTexImage2D(GL_TEXTURE_2D, 				// Type of texture
					 0,				// Pyramid level (for mip-mapping) - 0 is the top level
					 ilGetInteger(IL_IMAGE_BPP),	// Image colour depth
					 ilGetInteger(IL_IMAGE_WIDTH),	// Image width
					 ilGetInteger(IL_IMAGE_HEIGHT),	// Image height
					 0,				// Border width in pixels (can either be 1 or 0)
					 ilGetInteger(IL_IMAGE_FORMAT),	// Image format (i.e. RGB, RGBA, BGR etc.)
					 GL_UNSIGNED_BYTE,		// Image data type
					 ilGetData());			// The actual image data itself
 	}
  	else // If we failed to open the image file in the first place...
  	{
		error = ilGetError();
		std::cout << "Image load failed - IL reports error: " << error << " - " << iluErrorString(error) << std::endl;
		exit(-1);
  	}
 
 	ilDeleteImages(1, &imageID); // Because we have already copied image data into texture data we can release memory used by image.
 
	std::cout << "Texture creation successful." << std::endl;
 
	return textureID; // Return the GLuint to the texture so you can use it!
}

void DrawText(float x,float y, char *text)
{
    glPushMatrix();
    glTranslatef(x, y, 0);
    glScalef(0.0025,0.0025,0.0025);
    for( char* p = text; *p; p++)
    {
        glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);
    }
    glPopMatrix();
}
char * LongToString(long l)
{
std::ostringstream oss;
oss << l;
std::string str = oss.str();
// optionaly:
char *cstr = new char[str.length() + 1];
strcpy(cstr, str.c_str());
// do stuff



return cstr;
}
void InitMenu()
{
	polygons[0].xmin=200;
	polygons[0].xmax=570;
	polygons[0].ymin=150;
	polygons[0].ymax=205;

	polygons[1].xmin=200;
	polygons[1].xmax=570;
	polygons[1].ymin=245;
	polygons[1].ymax=300;

	polygons[2].xmin=200;
	polygons[2].xmax=570;
	polygons[2].ymin=338;
	polygons[2].ymax=393;

	polygons[3].xmin=200;
	polygons[3].xmax=570;
	polygons[3].ymin=430;
	polygons[3].ymax=485;

	polygons[0].x1=-2;
	polygons[0].x2=3;
	polygons[0].y1=1.25;
	polygons[0].y2=2.0;

	polygons[1].x1=-2;
	polygons[1].x2=3;
	polygons[1].y1=0;
	polygons[1].y2=0.75;

	polygons[2].x1=-2;
	polygons[2].x2=3;
	polygons[2].y1=-1.25;
	polygons[2].y2=-0.5;

	polygons[3].x1=-2;
	polygons[3].x2=3;
	polygons[3].y1=-2.5;
	polygons[3].y2=-1.75;
}



void Keyboard(unsigned char key, int x, int y)
{
  switch (key)
  {
  case 27:             // ESCAPE key
	  CurrentState=menu;
	  break;

  
  }
}
void DrawTextureSquare(float fSize,float z,GLuint texture)
{
  fSize /= 2.0;

  glBindTexture(GL_TEXTURE_2D,texture);
  glBegin(GL_QUADS);
  glTexCoord2f (0, 0);	glVertex3f(-fSize, -fSize,z);    
  glTexCoord2f (0, 1);	glVertex3f(-fSize, fSize,z);
  glTexCoord2f (1, 1);	glVertex3f(fSize, fSize,z);      
  glTexCoord2f (1, 0);	glVertex3f(fSize, -fSize,z);     
  glEnd();

}
void DrawTextureSquare(float fSize,GLuint texture)
{
  fSize /= 2.0;
  glEnable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,texture);
  glBegin(GL_QUADS);
  glTexCoord2f (0, 0);	glVertex2f(-fSize, -fSize);    
  glTexCoord2f (0, 1);	glVertex2f(-fSize, fSize);
  glTexCoord2f (1, 1);	glVertex2f(fSize, fSize);      
  glTexCoord2f (1, 0);	glVertex2f(fSize, -fSize);     
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}
void myMouseStoryScene()
{
	if(selectedMenu==0)
	{
		//PlaySound("sound/bubble.wav", NULL, SND_ASYNC|SND_FILENAME);
		//PlaySound("sound/menu.wav", NULL, SND_ASYNC|SND_FILENAME|SND_LOOP);
		if(curentStory+1==4)
		{
			mciSendString("close song1", NULL, 0, 0);
			CurrentState=start;
			ReSizeGLScene1(screen_width,screen_height);
			mciSendString("open sound/bg.mp3 type mpegvideo alias bg", NULL, 0, 0); 
			mciSendString("play bg repeat", NULL, 0, 0);
		}
		curentStory++;
		
		//	glScalef(1,1,1);

	}
}
void myMouseActHighScore() 
{

	if(selectedMenu==0)
	{
		//PlaySound("sound/bubble.wav", NULL, SND_ASYNC|SND_FILENAME);
		//PlaySound("sound/menu.wav", NULL, SND_ASYNC|SND_FILENAME|SND_LOOP);
		CurrentState=menu;
		//	glScalef(1,1,1);

	}
	else if(selectedMenu==1)
	{
		
		//PlaySound("intro.wav", NULL, SND_ASYNC|SND_FILENAME);
		//CurrentState=score;
		glScalef(1,1,1);
		//ReSizeGLScene1(screen_width,screen_height);	
	}
}

void myMouseActMenu() 
{
	/*if(selectedMenu==0)
	{
		mciSendString("close song1", NULL, 0, 0);
		//PlaySound("intro.wav", NULL, SND_ASYNC|SND_FILENAME);
		CurrentState=start;
		//glScalef(1,1,1);
		ReSizeGLScene1(screen_width,screen_height);
		mciSendString("open sound/bg.mp3 type mpegvideo alias bg", NULL, 0, 0); 
		mciSendString("play bg repeat", NULL, 0, 0);
	}*/
	if(selectedMenu==0)
	{
		
		//PlaySound("intro.wav", NULL, SND_ASYNC|SND_FILENAME);
		CurrentState=storyscene;
		//glScalef(1,1,1);
		
	}
	else if(selectedMenu==1)
	{
		mciSendString("open sound/bubble.wav type mpegvideo alias song2", NULL, 0, 0); 
		mciSendString("play song2", NULL, 0, 0);
		mciSendString("close song2", NULL, 0, 0);
		//PlaySound("sound/bubble.wav", NULL, SND_ASYNC|SND_FILENAME);
		CurrentState=help;
		//glScalef(1,1,1);
		//ReSizeGLScene1(screen_width,screen_height);	
	}
	else if(selectedMenu==2)
	{
		//PlaySound("sound/bubble.wav", NULL, SND_SYNC|SND_FILENAME);
		mciSendString("open sound/bubble.wav type mpegvideo alias song2", NULL, 0, 0); 
		mciSendString("play song2", NULL, 0, 0);
		mciSendString("close song2", NULL, 0, 0);
		CurrentState=highscore;
		//glScalef(1,1,1);
	}
	else if(selectedMenu==3)
	{
		mciSendString("open sound/bubble.wav type mpegvideo alias song2", NULL, 0, 0); 
		mciSendString("play song2", NULL, 0, 0);
		mciSendString("close song2", NULL, 0, 0);
		exit(0);
		//glScalef(1,1,1);
	}
}

void InitHighScore()
{
	for(int i=0;i<5;i++)
	{
		//scores[i].name="";
		//scores[i].score=0;
	}
	buttonHighScore[0].xmin=200;
	buttonHighScore[0].xmax=310;
	buttonHighScore[0].ymin=450;
	buttonHighScore[0].ymax=505;

	buttonHighScore[1].xmin=400;
	buttonHighScore[1].xmax=510;
	buttonHighScore[1].ymin=450;
	buttonHighScore[1].ymax=505;

	buttonHighScore[0].x1=-2;
	buttonHighScore[0].x2=-0.4;
	buttonHighScore[0].y1=-2;
	buttonHighScore[0].y2=-2.75;

	buttonHighScore[1].x1=0;
	buttonHighScore[1].x2=1.5;
	buttonHighScore[1].y1=-2;
	buttonHighScore[1].y2=-2.75;
}
void showHighScore()
{
	

	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity(); 
	
	//gluPerspective(50.0, 1.0, 3.0, 1000.0); 
	glScalef(0.25,0.25,0.25);
	//gluLookAt(0.0, 0.0, 86, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glScalef(10,10,10);

	glColor4f(1,1,1,0.5);

	

	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MenuFruitTexture);	
	glBegin(GL_QUADS);		
	glTexCoord2f(0.0f, 0.0f); 	glVertex2f(-4,   -4);
	glTexCoord2f(0.0f, 1.0f); 	glVertex2f(-4,   4);
	glTexCoord2f(1.0f, 1.0f); 	glVertex2f(4, 4);
	glTexCoord2f(1.0f, 0.0f); 	glVertex2f(4, -4);	
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	
	glColor4f(1,1,1,1);

	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,HighScoreTitleTexture);	
	glBegin(GL_QUADS);		
	glTexCoord2f(0.0f, 0.0f); 	glVertex2f(-5.85,   -9.0f);
	glTexCoord2f(0.0f, 1.0f); 	glVertex2f(-5.85,  4.0f);
	glTexCoord2f(1.0f, 1.0f); 	glVertex2f(7, 4.0f);	
	glTexCoord2f(1.0f, 0.0f); 	glVertex2f(7, -9.0f);	
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	

	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MenuItemsTexture);	
	for(int i=0;i<2;i++)
	{		
	glBegin(GL_QUADS);
		if(!buttonHighScore[i].over)
		{
		glTexCoord2f(0.0f, 0.56f);	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y1);
		glTexCoord2f(0.0f, 1.0f); 	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y2);
		glTexCoord2f(1.0f, 1.0f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y2);
		glTexCoord2f(1.0f, 0.56f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y1);			
		}
		else
		{
		glTexCoord2f(0.0f, 0.0f);	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y1+0.1);
		glTexCoord2f(0.0f, 0.55f); 	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y2+0.1);
		glTexCoord2f(1.0f, 0.55f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y2+0.1);
		glTexCoord2f(1.0f, 0.0f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y1+0.1);			
		}
	glEnd();
	}
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	DrawText(-1.75,-2.45,"Back");
	DrawText(0.15,-2.45,"Reset");

	//DrawText(-4,-1, LongToString(selectedMenu));
	//DrawText(-4,-2, LongToString(point.x));
	//DrawText(-4,-3, LongToString(point.y));
	
	//glLoadIdentity();u
	glTranslatef(-2,1.75,0);
	for(int i=0;i<5;i++)
	{
		DrawTextureSquare(0.5,numbers[i+1]);	
		glTranslatef(0.25,0,0);
		DrawTextureSquare(0.5,numbers[10]);	
		glTranslatef(-0.25,-0.65,0);
	}
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D); 
}
void showHelp()
{
	

	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity(); 
	
	//gluPerspective(50.0, 1.0, 3.0, 1000.0); 
	glScalef(0.25,0.25,0.25);
	//gluLookAt(0.0, 0.0, 86, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glScalef(10,10,10);
	glColor4f(1,1,1,0.5);
		

	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MenuFruitTexture);	
	glBegin(GL_QUADS);		
	glTexCoord2f(0.0f, 0.0f); 	glVertex2f(-4,   -4);
	glTexCoord2f(0.0f, 1.0f); 	glVertex2f(-4,   4);
	glTexCoord2f(1.0f, 1.0f); 	glVertex2f(4, 4);
	glTexCoord2f(1.0f, 0.0f); 	glVertex2f(4, -4);	
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1,1,1);

	glScalef(0.65,0.65,0.65);
	glTranslatef(-0.4,2.5,0);
	
	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,HelpTexture);	
	glBegin(GL_QUADS);		
	glTexCoord2f(0.0f, 0.0f); 	glVertex2f(-5.85,   -9.0f);
	glTexCoord2f(0.0f, 1.0f); 	glVertex2f(-5.85,  4.0f);
	glTexCoord2f(1.0f, 1.0f); 	glVertex2f(7, 4.0f);	
	glTexCoord2f(1.0f, 0.0f); 	glVertex2f(7, -9.0f);	
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glLoadIdentity(); 
	glScalef(0.25,0.25,0.25);


	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MenuItemsTexture);	
	for(int i=0;i<1;i++)
	{		
	glBegin(GL_QUADS);
		if(!buttonHighScore[i].over)
		{
		glTexCoord2f(0.0f, 0.56f);	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y1);
		glTexCoord2f(0.0f, 1.0f); 	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y2);
		glTexCoord2f(1.0f, 1.0f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y2);
		glTexCoord2f(1.0f, 0.56f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y1);			
		}
		else
		{
		glTexCoord2f(0.0f, 0.0f);	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y1+0.1);
		glTexCoord2f(0.0f, 0.55f); 	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y2+0.1);
		glTexCoord2f(1.0f, 0.55f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y2+0.1);
		glTexCoord2f(1.0f, 0.0f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y1+0.1);			
		}
	glEnd();
	}
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	DrawText(-1.75,-2.45,"Back");
	//DrawText(0.15,-2.45,"Reset");

	//DrawText(-4,-1, LongToString(selectedMenu));
	//DrawText(-4,-2, LongToString(point.x));
	//DrawText(-4,-3, LongToString(point.y));
	



}
void Menu()
{
	
	
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity(); 
	
	//gluPerspective(50.0, 1.0, 3.0, 1000.0); 
	glScalef(0.25,0.25,0.25);
	//gluLookAt(0.0, 0.0, 86, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glScalef(10,10,10);
	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MenuFruitTexture);	
	glBegin(GL_QUADS);		
	glTexCoord2f(0.0f, 0.0f); 	glVertex2f(-4,   -4);
	glTexCoord2f(0.0f, 1.0f); 	glVertex2f(-4,   4);
	glTexCoord2f(1.0f, 1.0f); 	glVertex2f(4, 4);
	glTexCoord2f(1.0f, 0.0f); 	glVertex2f(4, -4);	
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MainTitleTexture);	
	glBegin(GL_QUADS);		
	glTexCoord2f(0.0f, 0.0f); 	glVertex2f(-4.85,   -9.0f);
	glTexCoord2f(0.0f, 1.0f); 	glVertex2f(-4.85,  4.0f);
	glTexCoord2f(1.0f, 1.0f); 	glVertex2f(7, 4.0f);	
	glTexCoord2f(1.0f, 0.0f); 	glVertex2f(7, -9.0f);	
	glEnd();
	

	glBindTexture(GL_TEXTURE_2D,MenuItemsTexture);	
	for(int i=0;i<4;i++)
	{		
	glBegin(GL_QUADS);
		if(!polygons[i].over)
		{
		glTexCoord2f(0.0f, 0.56f);	glVertex2f(polygons[i].x1 , polygons[i].y1);
		glTexCoord2f(0.0f, 1.0f); 	glVertex2f(polygons[i].x1 , polygons[i].y2);
		glTexCoord2f(1.0f, 1.0f); 	glVertex2f(polygons[i].x2  , polygons[i].y2);
		glTexCoord2f(1.0f, 0.56f); 	glVertex2f(polygons[i].x2  , polygons[i].y1);			
		}
		else
		{
		glTexCoord2f(0.0f, 0.0f);	glVertex2f(polygons[i].x1 , polygons[i].y1-0.1);
		glTexCoord2f(0.0f, 0.55f); 	glVertex2f(polygons[i].x1 , polygons[i].y2-0.1);
		glTexCoord2f(1.0f, 0.55f); 	glVertex2f(polygons[i].x2  , polygons[i].y2-0.1);
		glTexCoord2f(1.0f, 0.0f); 	glVertex2f(polygons[i].x2  , polygons[i].y1-0.1);			
		}
	glEnd();
	}

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1,1,1);
	DrawText(-0.55,1.5,"Start");
	DrawText(-0.9,0.25,"Instruction");
	DrawText(-1.0,-1,"High Score");
	DrawText(-0.55,-2.25,"Exit");
		
	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);

	
	//DrawText(-4,-1, LongToString(selectedMenu));
	//DrawText(-4,-2, LongToString(point.x));
	//DrawText(-4,-3, LongToString(point.y));
	//DrawRectangleFill(0,0,0,2,2,2,2,0,1,1,1);
	//glColor3f(1,1,1);	
}
void game_start()
{
//glFogf(GL_FOG_MODE,GL_LINEAR);
		//glFogf(GL_FOG_DENSITY,curlvl.fogdensity);
		//glFogfv(GL_FOG_COLOR,curlvl.fogcolor);
		//glFogf(GL_FOG_START,curlvl.fogdist);
		//glFogf(GL_FOG_END,curlvl.fogdist+1.0f);
		//glEnable(GL_FOG);
		
		RenderSkybox();	
		
		for(int i=0;i<number_of_monster;i++) //Draw monster
		{
			if(list_monster[i].live==true)
			{
				if(list_monster[i].runframe_to!=0)
				{
					MoveMonster(i);
				}
				for(int w = 0; w<curlvl.width;w++)
					for(int h = 0; h<curlvl.height;h++)
						curlvl.monster_code[w][h]=0;
				for(int i=0;i<number_of_monster;i++)
				{
					curlvl.monster_code[(int)round(list_monster[i].zpos)][(int)round(list_monster[i].xpos)]=list_monster[i].monster_code;
				}			
				DrawMD2(i);	
			}
		}	

		for (int x=0;x<curlvl.height;x++)  //Draw Playground
			for (int y=0;y<curlvl.width;y++)
			{
				if(curlvl.t[y][x]==1)  //Wall 
				{
					glLoadIdentity();
					glRotatef(mainc.direction,0.0f,1.0f,0.0f);			
					glTranslatef(x-mainc.xpos,0.0f,(-y)-1.0f+mainc.zpos);
					glTranslatef(0.0f,mainc.ypos,0.0f);
					drawtile(deftiles[1]);			
				}
				if(curlvl.t[y][x]==2&&number_of_fruit>number_of_fruit_eaten)  //Door
				{
					glLoadIdentity();
					glRotatef(mainc.direction,0.0f,1.0f,0.0f);			
					glTranslatef(x-mainc.xpos,0.0f,(-y)-1.0f+mainc.zpos);
					glTranslatef(0.0f,mainc.ypos,0.0f);
					drawtile(deftiles[2]);			
				}
				else if (curlvl.t[y][x]==0||curlvl.t[y][x]==-1||curlvl.t[y][x]>5)  // 0 mean "have fruit" -1 mean "no fruit"
				{
					glLoadIdentity();
					glRotatef(mainc.direction,0.0f,1.0f,0.0f);			
					glTranslatef(x-mainc.xpos,0.0f,(-y)-1.0f+mainc.zpos);
					glTranslatef(0.0f,mainc.ypos,0.0f);
					drawtile(deftiles[0]);
						glDisable(GL_BLEND);
						glDisable(GL_TEXTURE_2D);
					if(curlvl.t[y][x]==0)  //Coin (Ammo)
					{
						Cube(0.07,x,y);
					}
					else if (curlvl.t[y][x]==6||curlvl.t[y][x]==11) //Weapon
					{
						glColor4f(1.0f,1.0f,0.0f,0.6f);

						DrawOBJ(0,"Model/lpxbananas.obj",0.3,x,y);

					}
					else if (curlvl.t[y][x]==7) //Weapon
					{
						glColor4f(0.2f,0.8f,0.1f,0.9f);
						DrawOBJ(1,"Model/lpxapple.obj",0.3,x,y);

					}
						glEnable(GL_BLEND);
						glEnable(GL_TEXTURE_2D);

				}
			}		
		if(gamemode==1&change_mode==false)
		{
			move1(fruitman);
			for(int i=0;i<number_of_monster;i++) //Check die
			{
				if((int)list_monster[i].zpos==(int)fruitman.zpos&&(int)list_monster[i].xpos==(int)fruitman.xpos&&list_monster[i].type<10&&list_monster[i].live==true)
				{
					fruitman.live--;

					fruitman.xpos=fruitman_xpos_defaut;
					fruitman.zpos=fruitman_zpos_defaut+1;
					fruitman.direction=0;
					
					camera_ypos=camera_ypos_default;
					camera_zpos=fruitman_zpos_defaut-camera_zpos_default;
					camera_xpos=fruitman.xpos+0.5f;
					mainc.direction=0;

					ReSizeGLScene1(screen_width,screen_height);
				}
			}	

		}
		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		float map_zoom=0.03f;		
		float up_offset=0.02f;
		float right_offset=0.0418f;
		//	glTranslatef(-1.0,camera_zpos/10+0.2,-0.5);
		glRotatef(-gamemode_camera_angle,1,0,0);
		//Draw maze map
		for(int i=0;i<=curlvl.width-1;i++)
			for(int j=0;j<=curlvl.height-1;j++)	
			{
				if(curlvl.t[i][j]==1||curlvl.t[i][j]==2)
				{
					glLoadIdentity();
					glRotatef(-gamemode_camera_angle,1,0,0);
					glTranslatef((j)*map_zoom/curlvl.width+right_offset,(i)*map_zoom/curlvl.width+up_offset,-0.1f);
				
					glBegin(GL_POLYGON);
						
						glColor3f(0.8f,0.80f,0.0f);	
						if(curlvl.t[i][j]==2)
						{
							glColor3f(1.0f,0.5f,0.0f);
						}
						glVertex2f(0, 0);        
						glVertex2f(0, map_zoom/curlvl.width);        
						glVertex2f(map_zoom/curlvl.width, map_zoom/curlvl.width);        
						glVertex2f(map_zoom/curlvl.width,0);    
					glEnd();
				}
			}
		//Finish draw maze map

		//Draw character on the MAP
		glLoadIdentity();	
		float char_radius=1.0f/max(curlvl.width,curlvl.height)*map_zoom/2;
		glRotatef(-gamemode_camera_angle,1,0,0);
		
		if(gamemode==1||change_mode==true)
		{			
			glTranslatef(map_zoom*(fruitman.xpos+0.5f)/curlvl.width+right_offset,map_zoom*(fruitman.zpos+0.5f)/curlvl.width+up_offset,-0.1f);	
			glRotatef(fruitman.direction,0.0f,0.0f,-1.0f);			
		}
		else
		{		
			glTranslatef(map_zoom*mainc.xpos/curlvl.width+right_offset,map_zoom*mainc.zpos/curlvl.width+up_offset,-0.1f);
			glRotatef(mainc.direction,0.0f,0.0f,-1.0f);			
		}
		glBegin(GL_POLYGON);  
			glColor3f(0.0f,1.0f,0.0f);	
			glVertex2f(-char_radius, char_radius);        
			glVertex2f(char_radius, char_radius);        
			glVertex2f(char_radius, -char_radius);        
			glVertex2f(-char_radius, -char_radius);    
		glEnd();

		glBegin(GL_LINES);    //Draw hand
			glColor3f(0.0f,1.0f,0.0f);	    
			glVertex2f(char_radius/2, char_radius);        
			glVertex2f(char_radius/2, 2*char_radius);   

			glVertex2f(-char_radius/2, char_radius);        
			glVertex2f(-char_radius/2, 2*char_radius);  
		glEnd();

		//finish character on the MAP

		//Draw monsters on the MAP
		for(int i=0;i<number_of_monster;i++)
		{
			if((list_monster[i].type==6||list_monster[i].type==7)&&list_monster[i].live==true)
			{
				glLoadIdentity();	
				float char_radius=1.0f/max(curlvl.width,curlvl.height)*map_zoom/2;
				glRotatef(-gamemode_camera_angle,1,0,0);
				glTranslatef(map_zoom*list_monster[i].xpos/curlvl.width+right_offset,map_zoom*list_monster[i].zpos/curlvl.width+up_offset,-0.1f);		
				//glRotatef(list_monster[i].direction,0.0f,0.0f,-1.0f);
				glBegin(GL_POLYGON);  				
					glColor3f(1.0f,0.0f,0.0f);	
					glVertex2f(0, 2*char_radius);        
					glVertex2f(2*char_radius, 2*char_radius);        
					glVertex2f(2*char_radius, 0);        
					glVertex2f(0, 0);    
				glEnd();
			}
		}
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		//finish monsters on the MAP
}
void scoringText()
{
	glLoadIdentity();
	
	glRotatef(-gamemode_camera_angle,1,0,0);
	glTranslatef(-1.1,0.3,-0.55);
	glColor3f(1,1,1);	
	
	DrawTextureSquare(4,-4,scoreetc);
	glTranslatef(-0.80,1.68,0);
	if(fruitman.live-1>=0)
		DrawTextureSquare(0.16,-4,numbers[fruitman.live-1]);
	else
		DrawTextureSquare(0.16,-4,numbers[0]);
	glTranslatef(-0.15,-0.21,0);

	if(number_of_fruit_eaten/10>0)
		DrawTextureSquare(0.16,-4,numbers[number_of_fruit_eaten/10]);
	glTranslatef(0.15,0,0);
	DrawTextureSquare(0.16,-4,numbers[number_of_fruit_eaten%10]);

	int temp=fruitman.ammo;
	int temp_hundreds=temp/100;
	temp%=100;
	int temp_ten=temp/10;
	temp%=10;

	glTranslatef(-0.30,-0.21,0);
	if(temp_hundreds>0)
		DrawTextureSquare(0.16,-4,numbers[temp_hundreds]);
	glTranslatef(0.15,0,0);
	if(temp_ten>=0)
		DrawTextureSquare(0.16,-4,numbers[temp_ten]);		
	glTranslatef(0.15,0,0);
	if(temp>=0)
		DrawTextureSquare(0.16,-4,numbers[temp]);
	
	 temp=score;
	 int temp_thousands=temp/1000;
	 temp%=1000;
	 temp_hundreds=temp/100;
	 temp%=100;
	 temp_ten=temp/10;
	 temp%=10;

	glTranslatef(-0.45,-0.21,0);
	if(temp_thousands>0)
		DrawTextureSquare(0.16,-4,numbers[temp_thousands]);		
	glTranslatef(0.15,0,0);
	if(temp_hundreds>=0)
		DrawTextureSquare(0.16,-4,numbers[temp_hundreds]);		
	glTranslatef(0.15,0,0);
	if(temp_ten>=0)
		DrawTextureSquare(0.16,-4,numbers[temp_ten]);		
	glTranslatef(0.15,0,0);
	if(temp>=0)
		DrawTextureSquare(0.16,-4,numbers[temp]);


	glRotatef(45,1,0,0);

	/*
				char buf1[200]; 
				char buf2[200]; 
				char buf3[200]; 
				char buf4[200]; 
				sprintf(buf1, "AMMO=%d",(int)fruitman.ammo);
				sprintf(buf2, "LIVE=%d",(int)fruitman.live);
				sprintf(buf3, "FRUIT=%d/%d",number_of_fruit_eaten,number_of_fruit);
				sprintf(buf4, "TIME=%d seconds",time_score);
				TextOut(hDC, 5,  5, buf1, strlen(buf1));
				TextOut(hDC, 5,  25, buf2, strlen(buf2));
				TextOut(hDC, 5,  45, buf3, strlen(buf3));
				TextOut(hDC, 5,  65, buf4, strlen(buf4));
				*/
}

void changelanguage()
{
	glLoadIdentity();	
	
	glRotatef(-gamemode_camera_angle,1,0,0);
	glTranslatef(-0.53,0.49,0.0f);
	glColor3f(1,1,1);	
	DrawTextureSquare(0.2,-1,languages[language_used]);	
}
void drawSquare(float size, float z)
{
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	size/=2;
	glBegin(GL_QUADS);
	glVertex3f(-size,-size,z);
	glVertex3f(-size,size,z);
	glVertex3f(size,size,z);
	glVertex3f(size,-size,z);
	glEnd();
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
}
void StoryScene()
{
	
	drawSquare(10,0);
	DrawTextureSquare(7,0,story[curentStory]);
	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D,MenuItemsTexture);	
	for(int i=0;i<1;i++)
	{		
	glBegin(GL_QUADS);
		if(!buttonHighScore[i].over)
		{
		glTexCoord2f(0.0f, 0.56f);	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y1);
		glTexCoord2f(0.0f, 1.0f); 	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y2);
		glTexCoord2f(1.0f, 1.0f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y2);
		glTexCoord2f(1.0f, 0.56f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y1);			
		}
		else
		{
		glTexCoord2f(0.0f, 0.0f);	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y1+0.1);
		glTexCoord2f(0.0f, 0.55f); 	glVertex2f(buttonHighScore[i].x1 , buttonHighScore[i].y2+0.1);
		glTexCoord2f(1.0f, 0.55f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y2+0.1);
		glTexCoord2f(1.0f, 0.0f); 	glVertex2f(buttonHighScore[i].x2  , buttonHighScore[i].y1+0.1);			
		}
	glEnd();
	}
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	if(curentStory<3)
		DrawText(-1.75,-2.45,"Next");
	else 
		DrawText(-1.75,-2.45,"Start");
}
int DrawGLScene(void)									
{		
	if(CurrentState==menu)
	{
		Menu();
	}
	else
	if(CurrentState==storyscene)
	{
		StoryScene();
	}
	else
	if (CurrentState==start)
	{
		play_eat();
		game_start();		
		scoringText();
		changelanguage();
		if(fruitman.zpos>17)
		{
			glLoadIdentity();	
			glRotatef(-gamemode_camera_angle,1,0,0);
			glTranslatef(0,0,0.0f);
			glColor3f(1,1,1);	
			DrawTextureSquare(1,-1,win);	
		}
	}	
	else
	if (CurrentState==highscore)
	{
		showHighScore();
	}
	else
	if (CurrentState==help)
	{
		showHelp();
	}
	return true;									
}
LRESULT CALLBACK WinProc(HWND hWnd,UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    LONG    lRet = 0; 
    PAINTSTRUCT    ps;

    switch (uMsg)
	{ 
    case WM_SIZE:										// If the window is resized
		if(gamemode==1)
		{
			ReSizeGLScene1(LOWORD(lParam),HIWORD(lParam));  		
		}
		else if(gamemode==2)
		{
			ReSizeGLScene1(LOWORD(lParam),HIWORD(lParam));  		
		}
	    break; 
 
	case WM_PAINT:										// If we need to repaint the scene
		BeginPaint(hWnd, &ps);							// Init the paint struct		
		EndPaint(hWnd, &ps);							// EndPaint, Clean up
		break;

	case WM_LBUTTONDOWN:								// If the left mouse button was clicked
		if(CurrentState==menu)
		{
			myMouseActMenu();
		}
		else if (CurrentState==highscore || CurrentState==help)
		{
			myMouseActHighScore();
		}
		else if (CurrentState==storyscene)
		{
			myMouseStoryScene();
		}
		
		break;

	case WM_RBUTTONDOWN:								// If the right mouse button was clicked.
		
		g_bLighting = !g_bLighting;						// Turn lighting ON/OFF

		if(g_bLighting) {								// If lighting is ON
			glEnable(GL_LIGHTING);						// Enable OpenGL lighting
		} else {
			glDisable(GL_LIGHTING);						// Disable OpenGL lighting
		}
		break;

	case WM_KEYDOWN:									// If we pressed a key

		switch(wParam) {								// Check if we hit a key
			case VK_ESCAPE:								// If we hit the escape key
				PostQuitMessage(0);						// Send a QUIT message to the window
				break;

			case VK_LEFT:								// If the LEFT arrow key was pressed
				g_RotationSpeed -= 0.05f;				// Decrease the rotation speed (eventually rotates left)
				break;

			case VK_RIGHT:								// If the RIGHT arrow key is pressed
				g_RotationSpeed += 0.05f;				// Increase the rotation speed (rotates right)
				break;
		}
		break;

    case WM_CLOSE:										// If the window is being closed
        PostQuitMessage(0);								// Send a QUIT Message to the window
        break; 
     case WM_MOUSEMOVE:
		{
			long x =	point.x = lParam & 0x0000ffff; // low order word = x
			long y =	point.y = lParam >> 16; // high order word = y
				
			if(CurrentState==menu)
			{
				for(int i=0;i<4;i++)
				{
					if(check_bounding(polygons[i], x,y))
					{
						selectedMenu=i;
						polygons[i].over=true;
						break;
					}
					else
					{
						selectedMenu=-1;
						polygons[i].over=false;

					}
				}
			}
			else if(CurrentState==highscore)
			{
				for(int i=0;i<2;i++)
				{
					if(check_bounding(buttonHighScore[i], x,y))
					{
						selectedMenu=i;
						buttonHighScore[i].over=true;
						break;
					}
					else
					{
						selectedMenu=-1;
						buttonHighScore[i].over=false;

					}
				}
			}
			else if(CurrentState==help)
			{
				for(int i=0;i<1;i++)
				{
					if(check_bounding(buttonHighScore[i], x,y))
					{
						selectedMenu=i;
						buttonHighScore[i].over=true;
						break;
					}
					else
					{
						selectedMenu=-1;
						buttonHighScore[i].over=false;

					}
				}
			}
			else if(CurrentState==storyscene)
			{
				for(int i=0;i<1;i++)
				{
					if(check_bounding(buttonHighScore[i], x,y))
					{
						selectedMenu=i;
						buttonHighScore[i].over=true;
						break;
					}
					else
					{
						selectedMenu=-1;
						buttonHighScore[i].over=false;

					}
				}
			}
		}
		break;
    default:											// Return by default
        lRet = DefWindowProc (hWnd, uMsg, wParam, lParam); 
        break; 
    } 
 
    return lRet;										// Return by default
}

void ReSizeGLScene1(GLsizei width, GLsizei height)	
{	
	glViewport(0,0,width,height);	
	glMatrixMode(GL_PROJECTION);	
	screen_height=height;
	screen_width=width;
	glLoadIdentity();

	mainc.direction=camera_direction;	
	mainc.zpos=camera_zpos;
	mainc.ypos=camera_ypos;	
	mainc.xpos=camera_xpos;	
	gluPerspective(55.0f,(GLfloat)width/(GLfloat)height,0.01f,100.0f);
	glRotatef(gamemode_camera_angle, 1.0f, 0.0f, 0.0f);	
	glMatrixMode(GL_MODELVIEW);		
}


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nShowCmd)
{
	MSG		msg;								
	BOOL	done=false;
	if (!CreateGLWindow("Fruit Man",SCREEN_WIDTH,SCREEN_HEIGHT,0,fullscreen))
	{
		return 0;								
	}
	for(int i=0;i<number_of_monster;i++)
	{
		g_LoadMd2.ImportMD2(&list_monster_3DModel[i], list_monster[i].model,10);		//Import MD2
		list_monster_3DModel[i].currentFrame=list_monster[i].runframe_from;
		temp_direction[i]=(rand()%4)*90; //randome one of 4 direction
	}
	g_LoadMd2.ImportMD2(&fruitman3DModel, fruitman.model,10);		//Import MD2
	


	int i=0;
	while(!done)								
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	
		{
			if (msg.message==WM_QUIT)				
			{
				mciSendString("close bg",NULL,0,0);
				done=true;						
			}
			else								
			{
				TranslateMessage(&msg);			
				DispatchMessage(&msg);		
			}
		}
		else								
		{
			if (active)							
			{
				if (GetAsyncKeyState(VK_ESCAPE)!=0)			
				{
					done=true;						
				}
				if (GetAsyncKeyState(VK_F2)!=0&&change_mode==false&&gamemode==2)			
				{
					gamemode=1;
					change_mode=true;
					gamemode_camera_angle_old=gamemode_camera_angle;
					gamemode_camera_angle_new=gamemode_camera_angle_default;

					camera_xpos_old=camera_xpos;
					camera_xpos_new=fruitman.xpos+0.5f;

					camera_ypos_old=camera_ypos;
					camera_ypos_new=camera_ypos_default;

					camera_zpos_old=camera_zpos;
					camera_zpos_new=fruitman.zpos-camera_zpos_default;
					camera_direction_old=mainc.direction;
					camera_direction_new=0;
					if(camera_direction_old>180)
					{
						camera_direction_old=camera_direction_old-360;
					}
					sky_rotate_old=sky_rotate;
					sky_rotate_new=50.0f;

					sky_ypos_old=sky_ypos;
					sky_ypos_new=mainc.ypos-scale-4.0f;

					sky_zpos_old=sky_zpos;
					sky_zpos_new=mainc.zpos/5-scale-14.0f;
					
				}
				if (GetAsyncKeyState(VK_F3)!=0&&change_mode==false&&gamemode==1)			
				{
					gamemode=2;
					change_mode=true;
					gamemode_camera_angle_old=gamemode_camera_angle;
					gamemode_camera_angle_new=0;
					camera_xpos_old=camera_xpos;
					camera_xpos_new=fruitman.xpos+0.5f;
					camera_ypos_old=camera_ypos;
					camera_ypos_new=-0.8f;
					camera_direction_old=mainc.direction;
					camera_direction_new=fruitman.direction;
					if(camera_direction_new>180)
					{
						camera_direction_new=camera_direction_new-360;
					}
					camera_zpos_old=camera_zpos;
					camera_zpos_new=fruitman.zpos+0.5;
					
					sky_rotate_old=sky_rotate;
					sky_rotate_new=90.0f;

					sky_ypos_old=sky_ypos;
					sky_ypos_new=0;

					sky_zpos_old=sky_zpos;
					sky_zpos_new=mainc.zpos/5-scale-23.0f;
				}
				if(change_mode)
				{
					i++;
					gamemode_camera_angle=gamemode_camera_angle_old+i*((gamemode_camera_angle_new-gamemode_camera_angle_old)/TIME_CHANGE_MODE);
					camera_xpos=camera_xpos_old+i*((camera_xpos_new-camera_xpos_old)/TIME_CHANGE_MODE);
					camera_ypos=camera_ypos_old+i*((camera_ypos_new-camera_ypos_old)/TIME_CHANGE_MODE);
					camera_zpos=camera_zpos_old+i*((camera_zpos_new-camera_zpos_old)/TIME_CHANGE_MODE);
					camera_direction=camera_direction_old+i*((camera_direction_new-camera_direction_old)/TIME_CHANGE_MODE);
					sky_rotate=sky_rotate_old+i*((sky_rotate_new-sky_rotate_old)/TIME_CHANGE_MODE);
					sky_ypos=sky_ypos_old+i*((sky_ypos_new-sky_ypos_old)/TIME_CHANGE_MODE);
					sky_zpos=sky_zpos_old+i*((sky_zpos_new-sky_zpos_old)/TIME_CHANGE_MODE);
					//Sleep(1);
					ReSizeGLScene1(screen_width,screen_height);
					if(i==TIME_CHANGE_MODE)
					{
						change_mode=false;
						i=0;
					}
				}	
				

				/*char buf4[200]; 
				char buf5[200]; 
				sprintf(buf4, "x=%d",(int)list_monster[5].xpos);
				sprintf(buf5, "z=%d",(int)list_monster[5].zpos);
				TextOut(hDC, 5,  85, buf4, strlen(buf4));
				TextOut(hDC, 5,  105, buf5, strlen(buf5));*/


				if (GetAsyncKeyState(VK_F4)!=0)		//Reset game Tam thoi dung thoi	
				{
					mainc=mainc_temp;
					score=0;
					fruitman=fruitman_temp;
					curlvl=curlvl_temp;
					gamemode=1;
					ReSizeGLScene1(screen_width,screen_height);	
					number_of_fruit_eaten=0;
				}
				if (GetAsyncKeyState(VK_F11)!=0)		//Change language
				{
					language_used=0;
				}
				if (GetAsyncKeyState(VK_F12)!=0)		//Change language
				{
					language_used=1;
				}
				if (GetAsyncKeyState(VK_F5)!=0)		//Change level
				{
					flood=true;	
					for(int i=0;i<=number_of_monster;i++)
					{
						list_monster[i].speed=list_monster[i].speed=0.005f;
					}
					
				}
				if (GetAsyncKeyState(VK_F6)!=0)		//Change level
				{
					flood=false;	
					for(int i=0;i<=number_of_monster;i++)
					{
						list_monster[i].speed=list_monster[i].speed=0.05f;
					}
				}
				else								
				{		
					//if(gamemode==1&&change_mode==false)
				//	{
						
				//	}
					if(gamemode==2&&change_mode==false)
					{
						move2(mainc);
					}						

					static int oldtime=timeGetTime();
					int time=timeGetTime();


					if ((time-oldtime)%1000==0)
						oldtime=time;
					//if ((time-oldtime)%(1000/30)==0)
					//{
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer																	
						DrawGLScene();
						SwapBuffers(hDC);
					//}

				}
			}
		}
	}
	
	// When we are done, we need to free all the model data
	// We do this by walking through all the objects and freeing their information

	for (int k=0;k<number_of_monster;k++)
	{
		for(int i = 0; i < list_monster_3DModel[k].numOfObjects; i++)
		{
			// Free the faces, normals, vertices, and texture coordinates.
			if(list_monster_3DModel[k].pObject[i].pFaces)		delete [] list_monster_3DModel[k].pObject[i].pFaces;
			if(list_monster_3DModel[k].pObject[i].pNormals)	delete [] list_monster_3DModel[k].pObject[i].pNormals;
			if(list_monster_3DModel[k].pObject[i].pVerts)		delete [] list_monster_3DModel[k].pObject[i].pVerts;
			if(list_monster_3DModel[k].pObject[i].pTexVerts)	delete [] list_monster_3DModel[k].pObject[i].pTexVerts;
		}
	}

	KillGLWindow();								
	return (msg.wParam);						
}