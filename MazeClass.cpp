
#include "MazeClass.h"	

//********************GENERATE MAZE*******************//
MazeClass::MazeClass()
{

}


int rand_int(int min, int max) //Return randome int number but different "min" number and "max" number, and...
{
	int range=(max-min)/2;
	int r=(rand() % (range));	
	return min+2*r+1;
}
void MazeClass::remove_door()
{
	for(int i=0;i<=MAZE_SIZE_ROW-1;i++)
		for(int j=0;j<=MAZE_SIZE_COL-1;j++)
			if(maze[i][j]==2)
				maze[i][j]=0;
}
void MazeClass::divide(int row_min,int col_min,int row_max,int col_max,int type) //type=0 => divide col, type=1 divide row
{
	int rand;
	if(type==0) //Hozizonal divider
	{
		if(row_max-row_min>3) //able to divide Vertical
		{
			if(col_max-col_min>2)
			{
				rand=rand_int(row_min+1,row_max-1);
				for(int i=col_min+1;i<=col_max-1;i++)
				{
					maze[rand][i]=1; //Create wall
				}
				if(maze[rand][col_min]==2) //is door
					maze[rand][col_min+1]=2; //Create a door
				if(maze[rand][col_max]==2)
					maze[rand][col_max-1]=2; //Create a door
				else
				{
					maze[rand][rand_int(col_min,col_max)]=2; //Create a door
				}
				divide(row_min,col_min,rand,col_max,1); 
				divide(rand,col_min,row_max,col_max,1);
			}
		}
	}
	else if(type==1) //Vertical divider
	{
		if(col_max-col_min>3) //able to divide Vertical
		{
			if(row_max-row_min>2)
			{

				rand=rand_int(col_min+1,col_max-1);		
				for(int i=row_min+1;i<=row_max-1;i++)
				{
					maze[i][rand]=1; //Create wall
				}
				if(maze[row_min][rand]==2) //is door
					maze[row_min+1][rand]=2;
				if(maze[row_max][rand]==2) //is door
					maze[row_max-1][rand]=2;
				else
				{
					maze[rand_int(row_min,row_max)][rand]=2; //Create a door
				}
				divide(row_min,col_min,row_max,rand,0); 
				divide(row_min,rand,row_max,col_max,0);
			}
		}
	}
	remove_door();
}
