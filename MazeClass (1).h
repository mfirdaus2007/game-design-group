
#include <windows.h>
#include <stdio.h>
#include <math.h>

#define MAZE_SIZE_ROW 11
#define MAZE_SIZE_COL 11

class MazeClass
{

	public:
		MazeClass();	
		void divide(int row_min,int col_min,int row_max,int col_max,int type);
		int maze[MAZE_SIZE_ROW][MAZE_SIZE_COL];
	private:
		void remove_door();
};